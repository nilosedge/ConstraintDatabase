package edu.southern.ConstraintDatabase.Visitors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;
import edu.southern.ConstraintDatabase.TreeNodes.AssignmentNode;
import edu.southern.ConstraintDatabase.TreeNodes.BoundNode;
import edu.southern.ConstraintDatabase.TreeNodes.ClauseNode;
import edu.southern.ConstraintDatabase.TreeNodes.ConstraintNode;
import edu.southern.ConstraintDatabase.TreeNodes.DBInfoNode;
import edu.southern.ConstraintDatabase.TreeNodes.ExpressionNode;
import edu.southern.ConstraintDatabase.TreeNodes.FactorNode;
import edu.southern.ConstraintDatabase.TreeNodes.PredicateNode;
import edu.southern.ConstraintDatabase.TreeNodes.StringNode;
import edu.southern.ConstraintDatabase.TreeNodes.SubjectNode;
import edu.southern.ConstraintDatabase.TreeNodes.TermNode;
import edu.southern.ConstraintDatabase.TreeNodes.VariableNode;
import edu.southern.ConstraintDatabase.TreeNodes.Mapping.TreeNodeMapper;
import edu.southern.ConstraintDatabase.enums.AssignmentType;

public class ConstraintNormalizationVisitor implements TreeNodeVisitor {
	
	public List<ConstraintNode> newConstraintNodes = new ArrayList<ConstraintNode>();
	private TreeNodeMapper mapper = new TreeNodeMapper(true);
	
	public void Visit(ClauseNode clauseNode) throws TreeNodeException {
		if(clauseNode.predicate != null) {
			clauseNode.predicate.Accept(this);
			PrinterVisitor xpr = new PrinterVisitor(System.out);
			clauseNode.Accept(xpr);
		}
	}

	public void Visit(PredicateNode predicateNode) throws TreeNodeException {
		for(ConstraintNode c: predicateNode.constraintNodes) {
			c.Accept(this);
		}
		predicateNode.constraintNodes = newConstraintNodes;
	}

	public void Visit(ConstraintNode constraintNode) {

		//XMLPrinterVisitor xml = new XMLPrinterVisitor(Console.Out);
		//constraintNode.Accept(xml);

		if(constraintNode.isNormalized()) {
			newConstraintNodes.add(constraintNode);
		} else {
			if(constraintNode.an.type == AssignmentType.EqualTo && !constraintNode.bn.varNode.sb) {
				try {
					String json = mapper.writeValueAsString(constraintNode);
					//System.out.println("Mapped: " + json);
					ConstraintNode copy = (ConstraintNode)mapper.readValue(json, ConstraintNode.class);
					constraintNode.an.type = AssignmentType.GreaterThanOrEqualTo;
					copy.an.type = AssignmentType.GreaterThanOrEqualTo;
					copy.makeNegative();
					newConstraintNodes.add(copy);
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
			if(constraintNode.an.type == AssignmentType.NotEqualTo) {
				try {
					String json = mapper.writeValueAsString(constraintNode);
					//System.out.println("Mapped: " + json);
					ConstraintNode copy = (ConstraintNode)mapper.readValue(json, ConstraintNode.class);
					constraintNode.an.type = AssignmentType.GreaterThan;
					copy.an.type = AssignmentType.GreaterThan;
					copy.makeNegative();
					newConstraintNodes.add(copy);
				} catch (JsonGenerationException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(constraintNode.an.type == AssignmentType.LessThanOrEqualTo) {
				constraintNode.an.type = AssignmentType.GreaterThanOrEqualTo;
				constraintNode.makeNegative();
			}
			if(constraintNode.an.type == AssignmentType.LessThan) {
				constraintNode.an.type = AssignmentType.GreaterThan;
				constraintNode.makeNegative();
			}
			newConstraintNodes.add(constraintNode);
		}
	}
	
	public void Visit(SubjectNode subjectNode) {

	}
	public void Visit(DBInfoNode dbinfoNode) {

	}
	public void Visit(BoundNode boundNode) {

	}
	public void Visit(ExpressionNode expessionNode) {

	}
	public void Visit(TermNode termNode) {

	}
	public void Visit(StringNode stringNode) {

	}
	public void Visit(VariableNode varNode) {

	}
	public void Visit(AssignmentNode assignNode) {

	}
	public void Visit(FactorNode factorNode) {

	}
}
