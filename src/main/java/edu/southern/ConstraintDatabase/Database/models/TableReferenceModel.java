package edu.southern.ConstraintDatabase.Database.models;

import java.util.ArrayList;
import java.util.List;

import edu.southern.ConstraintDatabase.Interfaces.DatabaseDAOVisitor;

public class TableReferenceModel extends DatabaseModel {

	public long id;
	public long rowId;
	public long tableId;
	public TableModel tableRef;
	
	public List<ColumnMappingModel> columnmappings = new ArrayList<ColumnMappingModel>();
	
	public TableReferenceModel() {
	}
	
	public void Accept(DatabaseDAOVisitor visitor) {
		visitor.Visit(this);
	}
}
