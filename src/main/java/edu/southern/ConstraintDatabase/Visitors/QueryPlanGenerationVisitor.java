package edu.southern.ConstraintDatabase.Visitors;

import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import edu.southern.ConstraintDatabase.Database.DatabaseManager;
import edu.southern.ConstraintDatabase.Database.models.ColumnMappingModel;
import edu.southern.ConstraintDatabase.Database.models.ColumnModel;
import edu.southern.ConstraintDatabase.Database.models.ConstraintModel;
import edu.southern.ConstraintDatabase.Database.models.RowModel;
import edu.southern.ConstraintDatabase.Database.models.TableModel;
import edu.southern.ConstraintDatabase.Database.models.TableReferenceModel;
import edu.southern.ConstraintDatabase.Interfaces.DatabaseDAOVisitor;

public class QueryPlanGenerationVisitor extends PrinterUtil implements DatabaseDAOVisitor {

	private DatabaseManager dm;
	//private HashMap<String, String> graph = new HashMap<String, String>();
	private HashMap<String, HashSet<String>> graph = new HashMap<String, HashSet<String>>();
	
	private Deque<String> stack = new LinkedList<String>();
	
	public QueryPlanGenerationVisitor(DatabaseManager dm) {
		super(System.out);
		this.dm = dm;
	}
	
	public void Visit(TableModel table) {
		
		String tableName = table.name + "(";
		int count = 0;
		List<ColumnModel> columns = dm.getColumns(table.id);
		for(ColumnModel c: columns) {
			tableName += c.name;
			if(count < columns.size() - 1) tableName += ",";
			count++;
		}
		tableName += ")";
		
		//PrintLine(tableName);
		//indent();
		
		if(!stack.contains(tableName)) {
			
			if(graph.isEmpty()) {
				putEdge("Initial", tableName);
			} else {
				putEdge(stack.peekLast(), tableName);
			}
			stack.addLast(tableName);
			for(RowModel r: dm.getRows(table.id)) {
				r.Accept(this);
			}
			stack.removeLast();
		} else {
			if(graph.isEmpty()) {
				putEdge("Initial", tableName);
			} else {
				putEdge(stack.peekLast(), tableName);
			}
		}
		//unindent();
		
		
	}

	public void Visit(RowModel row) {
		for(TableReferenceModel trm: dm.getTableRefs(row.id)) {
			trm.Accept(this);
		}
	}

	public void Visit(TableReferenceModel tableReference) {
		TableModel t = dm.getTableById(tableReference.tableId);
		t.Accept(this);
	}

	public String getDotString() {
		String ret = "digraph G {\n";
		for(String s: graph.keySet()) {
			for(String s2: graph.get(s)) {
				ret += "\t\"" + s + "\" -> \"" + s2 + "\";\n";
			}
		}
		ret += "}\n";
		return ret;
	}
	
	private void putEdge(String key, String val) {
		//System.out.println("Adding: " + key + " -> " + val);
		//System.out.println("Stack: " + stack);
		if(graph.get(key) == null) {
			graph.put(key, new HashSet<String>());
		}
		if(!graph.get(key).contains(val)) {
			graph.get(key).add(val);
		}
	}
	
	public void Visit(ColumnModel column) {}
	public void Visit(ColumnMappingModel columnMapping) {}
	public void Visit(ConstraintModel constraint) {}
}