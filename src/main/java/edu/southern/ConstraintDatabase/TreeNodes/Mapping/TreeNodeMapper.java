package edu.southern.ConstraintDatabase.TreeNodes.Mapping;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.DeserializationConfig;

public class TreeNodeMapper extends ObjectMapper {

	private boolean debug = false;
	
	public TreeNodeMapper(boolean debug) {
		this.debug = debug;
		init();
	}
	
	private void init() {
		this.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
		this.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, debug);
	}
}
