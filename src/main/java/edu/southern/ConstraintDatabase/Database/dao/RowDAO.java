package edu.southern.ConstraintDatabase.Database.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.southern.ConstraintDatabase.Database.models.RowModel;
import edu.southern.ConstraintDatabase.Database.util.SQLiteConnection;

public class RowDAO extends SQLiteDAO {

	public RowDAO(SQLiteConnection con) {
		super(con);
		sqlTableName = "rows";
	}
	
	public List<RowModel> getRowsByTableId(long tableId) {
		List<RowModel> list = new ArrayList<RowModel>();
		ResultSet set = runQuery("select * from " + sqlTableName + " where tableId = " + tableId + " order by id");
		try {
			if(set.isBeforeFirst()) {
				while(set.next()) {
					list.add(populate(set));
				}
			}
			closeResultSet(set);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return list;
	}
	
	public RowModel getRowById(long id) {
		ResultSet set = getRecordById(id);
		RowModel r = populate(set);
		closeResultSet(set);
		return r;
	}
	
	public RowModel createRow(long tableId) {
		runUpdateQuery("insert into " + sqlTableName + " (tableId) values(" + tableId + ")");
		ResultSet set = runQuery("select * from " + sqlTableName + " where id = last_insert_rowid()");
		RowModel r = null;
		try {
			if(set.isBeforeFirst()) {
				while(set.next()) {
					r = populate(set);
				}
			}
			closeResultSet(set);
		} catch (SQLException e) {
			r = null;
			e.printStackTrace();
		}
		return r;
	}
	
	public void deleteRowsByTableId(long tableId) {
		runUpdateQuery("delete from " + sqlTableName + " where tableId=" + tableId);
	}
	
	private RowModel populate(ResultSet set) {
		RowModel r = new RowModel();
		try {
			r.id = set.getLong(1);
			r.tableId = set.getLong(2);
		} catch (SQLException e) {
			e.printStackTrace();
			r = null;
		}
		return r;
	}
}
