package edu.southern.ConstraintDatabase.TreeNodes;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;

public class FactorNode extends TreeNode {

	public VariableNode v;
	public ExpressionNode e;
	
	public FactorNode() {
	}
	
	public FactorNode(VariableNode v) {
		this.v = v;
	}
	
	public FactorNode(ExpressionNode e) {
		this.e = e;
	}

	@Override
	public void Accept(TreeNodeVisitor visitor) throws TreeNodeException {
		visitor.Visit(this);
	}
}
