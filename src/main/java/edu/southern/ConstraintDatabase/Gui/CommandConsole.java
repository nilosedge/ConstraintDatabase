package edu.southern.ConstraintDatabase.Gui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Logic.LogicHandler;
import edu.southern.ConstraintDatabase.TreeNodes.ClauseNode;

public class CommandConsole {

	private LogicHandler logic = null;
	
	public CommandConsole(LogicHandler lh) {
		logic = lh;
		
		System.out.println("type '@' to quit...");

		while(true) {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
			String line = "";
			try {
				line = bufferedReader.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(line.contains("@")) {
				break;
			}
			if(line.length() > 0) {
				ClauseNode c = logic.parseLine(line);
				if(c != null) {
					try {
						logic.semanticCheck(c);
						logic.normalizeClauseNode(c);
						logic.saveClauseNode(c);
					} catch (TreeNodeException e) {
						System.out.println(e.getMessage());
						e.printStackTrace();
					}
					
				}
			} else {
				// Do nothing
			}
		}
	}
}
