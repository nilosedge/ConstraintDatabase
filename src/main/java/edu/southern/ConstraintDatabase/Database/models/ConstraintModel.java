package edu.southern.ConstraintDatabase.Database.models;

import edu.southern.ConstraintDatabase.Interfaces.DatabaseDAOVisitor;
import edu.southern.ConstraintDatabase.TreeNodes.ConstraintNode;

public class ConstraintModel extends DatabaseModel {

	public long id;
	public long rowId;
	public String dataField;
	
	public ConstraintNode constraintNode;
	
	public ConstraintModel() {
	}

	public void Accept(DatabaseDAOVisitor visitor) {
		visitor.Visit(this);
	}
}
