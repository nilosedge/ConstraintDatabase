package edu.southern.ConstraintDatabase.Exceptions;

public class TreeNodeException extends Exception {
	private static final long serialVersionUID = 3272299683078080723L;

	public TreeNodeException(String str) {
		super(str);
	}
}
