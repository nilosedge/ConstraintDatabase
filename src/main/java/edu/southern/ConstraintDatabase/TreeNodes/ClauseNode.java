package edu.southern.ConstraintDatabase.TreeNodes;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;

public class ClauseNode extends TreeNode {

	public SubjectNode subject;
	public PredicateNode predicate;
	
	public ClauseNode() {
	}
	
	public ClauseNode(SubjectNode subject, PredicateNode predicate) {
		this.subject = subject;
		this.predicate = predicate;
	}
	
	public boolean isFact() {
		return predicate.isFact();
	}

	@Override
	public void Accept(TreeNodeVisitor visitor) throws TreeNodeException {
		visitor.Visit(this);
	}
}
