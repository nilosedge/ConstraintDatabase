grammar Grammer;

options {
  language=Java;
}

tokens {
	PLUS 	= '+' ;
	MINUS	= '-' ;
	MULT	= '*' ;
	DIV	= '/' ;


	RPAREN	= ')' ;
	LPAREN	= '(' ;
	COMMA		= ',' ;
	
	GEQ     = '>=' ;
	LEQ     = '<=' ;
	EQ      = '=' ;
	LT      = '<' ;
	GT      = '>' ;
	NEQ     = '!=' ;
}

@header {
package edu.southern.ConstraintDatabase.Grammer;

import edu.southern.ConstraintDatabase.TreeNodes.*;
import edu.southern.ConstraintDatabase.enums.AssignmentType;
import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
}

@lexer::header {
package edu.southern.ConstraintDatabase.Grammer;
}

@members {
@Override
public void emitErrorMessage(String msg) {
	//super.emitErrorMessage(msg);
}

@Override
public void displayRecognitionError(String[] tokenNames, RecognitionException e) {
	super.displayRecognitionError(tokenNames, e);
	String hdr = getErrorHeader(e);
	String msg = getErrorMessage(e, tokenNames);
	String errorMessage = "";
	for (int i = 0; i < e.charPositionInLine; i++) errorMessage += " ";
	errorMessage += "^";
	System.out.println(errorMessage);
	System.out.println("Parser: An error occured on: " + hdr + " " + msg + "");
}
}

@lexer::members {
@Override
public void emitErrorMessage(String msg) {
	//super.emitErrorMessage(msg);
}

@Override
public void displayRecognitionError(String[] tokenNames, RecognitionException e) {
	super.displayRecognitionError(tokenNames, e);
	String hdr = getErrorHeader(e);
	String msg = getErrorMessage(e, tokenNames);
	String errorMessage = "";
	for (int i = 0; i < e.charPositionInLine; i++) errorMessage += " ";
	errorMessage += "^";
	System.out.println(errorMessage);
	System.out.println("Lexer: An error occured on: " + hdr + " " + msg + "");
}
}

   
public clause returns[ClauseNode clause]:
   s=subject ':-' p=predicate '.' NEWLINE? { clause = new ClauseNode(s, p); } |
   s=subject '.' NEWLINE? { clause = new ClauseNode(s, null); }
   ;

subject returns[SubjectNode subject]:
	db=dbinfo { return new SubjectNode(db); }
	;

predicate returns[PredicateNode predicate] @init { predicate = new PredicateNode(); }:
	n1=node[predicate] (COMMA n2=node[predicate])*
	;

node [PredicateNode p]:
   db=dbinfo { p.addDBInfo(db); }
	| co=constraint { p.addConstraint(co); }
	;

dbinfo returns[DBInfoNode dbinfo]:
   vn=variable_name LPAREN vl=var_list RPAREN { return new DBInfoNode(vn, vl); }
	;
  
//var_list:	
//   var_or_number(',' var_or_number)*;
var_list returns[List<VariableNode> list] @init { list = new ArrayList<VariableNode>(); }:
   //variable { var_list.addVariable(variable()); } (COMMA variable { var_list.addVariable(variable()); })*
   v1=variable { list.add(v1); }
		(
			COMMA v2=variable { list.add(v2); }
		)* // { return new VariableListNode(); }
	;

variable returns[VariableNode variable]:
	vname=variable_name { variable = vname; }
	| vnum=variable_number { variable = vnum; }
	;

constraint returns[ConstraintNode constraint]:
   e=expr a=assign b=bound { return new ConstraintNode(e, a, b); }
	;

assign returns[AssignmentNode assign]:
	GT { assign = new AssignmentNode(AssignmentType.GreaterThan); }
	| LT { assign = new AssignmentNode(AssignmentType.LessThan); }
	| GEQ { assign = new AssignmentNode(AssignmentType.GreaterThanOrEqualTo); }
	| LEQ { assign = new AssignmentNode(AssignmentType.LessThanOrEqualTo); }
	| EQ { assign = new AssignmentNode(AssignmentType.EqualTo); }
	| NEQ { assign = new AssignmentNode(AssignmentType.NotEqualTo); }
	;


expr returns[ExpressionNode expr] @init { expr = new ExpressionNode(); boolean neg=false; }:
	t1=term { expr.addTermNode(t1); }
		(
			(PLUS | MINUS { neg=true; }) t2=term { t2.neg = neg; expr.addTermNode(t2); }
		)* 
	;

term returns[TermNode term] @init { term = new TermNode(); boolean mult=false; }:
	f1=factor {term.addFirstFactorNode(f1); }
		(
			(MULT { mult=true; } | DIV) f2=factor { if(mult) term.addFactorNode(f2, "*"); else term.addFactorNode(f2, "/"); }
		)*
	;

factor returns[FactorNode factor]:
	MINUS v1=variable_name { v1.neg = true; factor = new FactorNode(v1); }
	| v2=variable_name { factor = new FactorNode(v2); }
	| MINUS v3=variable_number { v3.neg = true; factor = new FactorNode(v3); }
	| v4=variable_number { factor = new FactorNode(v4); }
	| LPAREN e=expr RPAREN { factor = new FactorNode(e); }
	;

bound returns[BoundNode bound] @init { boolean neg=false; }:
	(MINUS { neg=true; })? n1=variable_number { n1.neg=neg; bound = new BoundNode(n1); }
	| n2=string { n2.sb = true; bound = new BoundNode(n2); }
	;

variable_number returns[VariableNode variable_number]:
	num=number { variable_number = new VariableNode($num.text, true); };

variable_name returns[VariableNode variable_name]:
	name=VARNAME { variable_name = new VariableNode($name.text, false); };
   
number:
	DIGIT+ '.' DIGIT+ | '.'? DIGIT+;

DIGIT: ('0'..'9');

VARNAME:
	('a'..'z'|'A'..'Z')('a'..'z'|'A'..'Z'|'0'..'9')*;
  
NEWLINE:
	'\r'?'\n';
WS:
	(' '|'\t'|'\n'|'\r')+ { $channel=HIDDEN; };

string returns[VariableNode variable_string]:
	q=QUOTED { return new VariableNode($q.text, false); }
	;

QUOTED:	'"' (~'"')* '"';
