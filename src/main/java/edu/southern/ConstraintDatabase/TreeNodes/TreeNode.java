package edu.southern.ConstraintDatabase.TreeNodes;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;

public abstract class TreeNode {
	public abstract void Accept(TreeNodeVisitor visitor) throws TreeNodeException;
	
	public TreeNode() {
	}
}
