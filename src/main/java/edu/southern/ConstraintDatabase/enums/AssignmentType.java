package edu.southern.ConstraintDatabase.enums;

public enum AssignmentType {
	EqualTo(1, "=") { },
	GreaterThan(2, ">") { },
	GreaterThanOrEqualTo(3, ">=") { },
	LessThan(4, "<") { },
	LessThanOrEqualTo(5, "<=") { },
	NotEqualTo(6, "!=") { },
	UNKNOWN(7, "?") { },
	;
	
	private int id;
	private String oper;
	
	AssignmentType(int id, String oper) {
		this.id = id;
		this.oper = oper;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOper() {
		return oper;
	}
	public void setOper(String oper) {
		this.oper = oper;
	}
}
