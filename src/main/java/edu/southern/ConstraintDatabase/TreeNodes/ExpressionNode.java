package edu.southern.ConstraintDatabase.TreeNodes;

import java.util.ArrayList;
import java.util.List;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;

public class ExpressionNode extends TreeNode {

	public List<TermNode> terms = new ArrayList<TermNode>();
	
	public ExpressionNode() {
	}
	
	public void addTermNode(TermNode term) {
		terms.add(term);
	}
	
	public void makeNegative() {
		for(TermNode t: terms) {
			t.neg = !t.neg;
		}
	}

	@Override
	public void Accept(TreeNodeVisitor visitor) throws TreeNodeException {
		visitor.Visit(this);
	}
}
