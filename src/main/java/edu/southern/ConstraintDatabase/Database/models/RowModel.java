package edu.southern.ConstraintDatabase.Database.models;

import java.util.ArrayList;
import java.util.List;

import edu.southern.ConstraintDatabase.Interfaces.DatabaseDAOVisitor;

public class RowModel extends DatabaseModel {

	public long id;
	public long tableId;
	public List<TableReferenceModel> refs = new ArrayList<TableReferenceModel>();
	public List<ConstraintModel> constraints = new ArrayList<ConstraintModel>();
	
	public RowModel() {
	}

	public void Accept(DatabaseDAOVisitor visitor) {
		visitor.Visit(this);
	}
}
