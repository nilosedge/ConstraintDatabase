package edu.southern.ConstraintDatabase.Gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import edu.southern.ConstraintDatabase.GraphUtils.GraphDisplay;
import edu.southern.ConstraintDatabase.Logic.LogicHandler;
import edu.southern.ConstraintDatabase.TreeNodes.ClauseNode;

public class MainFrame extends JFrame {
	
	private static final long serialVersionUID = 3753928291720724433L;
	private LogicHandler logic;
	private DefaultMutableTreeNode root;
	private JTree tree;
	private JFrame thisFrame = this;
	private JTextArea typeArea;
	private JTextPane consoleArea;

	public MainFrame(LogicHandler lh) {
		super("Constraint Database");
		this.logic = lh;
		setTitle("Constraint Database: " + logic.currentDatabase);
		setLayout(new BorderLayout());
		setMinimumSize(new Dimension(1024, 600));
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				exitProcedure();
			}
		});
		
		setupTree();
		JScrollPane pane = new JScrollPane(tree);
		pane.setPreferredSize(new Dimension(250, 600));
		add(pane, BorderLayout.WEST);
		
		JPanel panel = new JPanel(new GridLayout(2, 1));
		typeArea = new JTextArea();
		typeArea.setLineWrap(true);
		panel.add(typeArea);
		
		
		consoleArea = new JTextPane();
		consoleArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
		JScrollPane pane2 = new JScrollPane(consoleArea);
		panel.add(pane2);
		MessageConsole mc = new MessageConsole(consoleArea, true);
		mc.redirectOut();
		mc.redirectErr(Color.RED, System.out);
		mc.setMessageLines(1000);
		
		add(panel, BorderLayout.CENTER);
		
		JPanel buttons = new JPanel(new GridLayout(20, 1));
		
		JButton parse = new JButton("Parse Statements");
		
		buttons.add(parse);
		parse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] lines = typeArea.getText().split("\n");
				List<ClauseNode> clauses = logic.parseLines(lines);
				clauses.size();
			}
		});
		JButton semcheck = new JButton("Semantic Check");
		semcheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				String[] lines = typeArea.getText().split("\n");
				List<ClauseNode> clauses = logic.parseLines(lines);
				if(clauses != null) {
					logic.semanticCheck(clauses);
				}
			}
		});
		buttons.add(semcheck);
		JButton save = new JButton("Save");
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				String[] lines = typeArea.getText().split("\n");
				List<ClauseNode> clauses = logic.parseLines(lines);
				if(clauses != null) {
					logic.semanticCheck(clauses);
					logic.normalize(clauses);
					List<String> createdTables = logic.save(clauses);
					boolean added = false;
					for(int i = 0; i < root.getChildCount(); i++) {
						DefaultMutableTreeNode child = (DefaultMutableTreeNode) root.getChildAt(i);
						if(child.toString().equals(logic.currentDatabase)) {
							for(String s: createdTables) {
								boolean found = false;
								for(int j = 0; j < child.getChildCount(); j++) {
									DefaultMutableTreeNode baby = (DefaultMutableTreeNode) child.getChildAt(j);
									if(baby.toString().equals(s)) { found = true; break; }
								}
								if(!found) {
									child.add(new DefaultMutableTreeNode(s));
									added = true;
								}
							}
							break;
						}
					}
					if(added) {
						tree.setModel(new DefaultTreeModel(root));
					}
				}
			}
		});
		
		
		buttons.add(save);
		
		JButton clearConsole = new JButton("Clear Console");
		clearConsole.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				consoleArea.setText("");
			}
		});
		buttons.add(clearConsole);
		
		JButton exitProgram = new JButton("Exit Program");
		exitProgram.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exitProcedure();
			}
		});
		buttons.add(exitProgram);
		
		
		add(buttons, BorderLayout.EAST);
		
	}
	
	private JTree setupTree() {

		List<String> list = logic.getDatabaseFiles();
		root = new DefaultMutableTreeNode("SQLite Files");
		for(String s: list) {
			DefaultMutableTreeNode child = new DefaultMutableTreeNode(s);
			logic.selectDatabase(s);
			List<String> tables = logic.getTables();
			for(String t: tables) {
				child.add(new DefaultMutableTreeNode(t));
			}
			root.add(child);
		}
		tree = new JTree(root);
		
		tree.addMouseListener(getMouseAdapter());
		return tree;
	}

	
	private MouseAdapter getMouseAdapter() {
		return new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				int selRow = tree.getRowForLocation(e.getX(), e.getY());
				TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
				if(selRow != -1) {
					if(e.getClickCount() == 1) {
						if(e.getButton() == MouseEvent.BUTTON1) {
							if(selPath.getPathCount() == 1) {
								System.out.println("Root Node: Does Nothing");
							}
							if(selPath.getPathCount() == 2) {
								logic.selectDatabase(selPath.getPath()[1].toString());
								setTitle("Constraint Database: " + logic.currentDatabase);
							}
							if(selPath.getPathCount() == 3) {
								System.out.println("Table: " + selPath.getPath()[2].toString());
								logic.selectDatabase(selPath.getPath()[1].toString());
								setTitle("Constraint Database: " + logic.currentDatabase);
								logic.getTableRows(selPath.getPath()[2].toString());
							}

						} else if(e.getButton() == MouseEvent.BUTTON3) {
							if(selPath.getPathCount() == 1) {
								String newDatabase = JOptionPane.showInputDialog("Please Enter the name of a new Database:");
								if(newDatabase != null && newDatabase.length() > 0) {
									newDatabase = newDatabase.replace(".sqlite", "");
									newDatabase = newDatabase.replace(" ", "");
									logic.selectDatabase(newDatabase + ".sqlite");
									root.add(new DefaultMutableTreeNode(newDatabase + ".sqlite"));
									setTitle("Constraint Database: " + logic.currentDatabase);
									tree.setModel(new DefaultTreeModel(root));
								}

							}
							if(selPath.getPathCount() == 2) {
								int n = JOptionPane.showConfirmDialog(
									    thisFrame,
									    "Are you sure you want to delete: " + selPath.getPath()[1].toString() + "?",
									    "Delete Confirmation",
									    JOptionPane.YES_NO_OPTION);
								if(n == 0) {
									String filename = selPath.getPath()[1].toString();
									//System.out.println("Removing Node: " + filename);
									logic.deleteDatabase(filename);
									for(int i = 0; i < root.getChildCount(); i++) {
										if(root.getChildAt(i).toString().equals(filename)) {
											root.remove(i);
											break;
										}
									}
									tree.setModel(new DefaultTreeModel(root));
								}
							}
							if(selPath.getPathCount() == 3) {
								logic.selectDatabase(selPath.getPath()[1].toString());
								setTitle("Constraint Database: " + logic.currentDatabase);
								String tableName = selPath.getPath()[2].toString();
								String filePath = logic.generateQueryPlan(tableName);
								GraphDisplay g = new GraphDisplay(filePath);
								g.setVisible(true);
								
								//System.out.println("Show Graph of Query Plan for: " + tableName);
							}

						}
					}
					
					// Double Clicks
					else if(e.getClickCount() == 2) {
						if(e.getButton() == MouseEvent.BUTTON1) {
							if(selPath.getPathCount() == 1) {
								String name = selPath.getPath()[0].toString();
								System.out.println("Collapses or Opens: " + name);
							}
							if(selPath.getPathCount() == 2) {
								String name = selPath.getPath()[1].toString();
								System.out.println("Collapses or Opens: " + name);
							}
							if(selPath.getPathCount() == 3) {
								String tableName = selPath.getPath()[2].toString();
								System.out.println("pop a dialog giving options to run a query for table: " + tableName);
							}
						} else if(e.getButton() == MouseEvent.BUTTON3) {
							System.out.println("Does Nothing");
						}
						
						
					}
				}
			}
		};
	}
	
	private void exitProcedure() {
		System.exit(0);
	}

}
