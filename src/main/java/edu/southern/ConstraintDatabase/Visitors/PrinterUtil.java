package edu.southern.ConstraintDatabase.Visitors;

import java.io.PrintStream;

public class PrinterUtil {

	private int indentAmount = 5;
	private int indentSpace = 0;
	
	protected PrintStream out;
	
	public PrinterUtil(PrintStream out) {
		this.out = out;
	}

	public void PrintLine (String str) {
		for (int i = 0; i < indentAmount * indentSpace; i++) {
			out.print(" ");
		}
		out.println(str);
	}
	
	public void Print (String str) {
		for (int i = 0; i < indentAmount * indentSpace; i++) {
			out.print(" ");
		}
		out.print(str);
	}

	public void indent() { indentSpace++; }
	public void unindent() { indentSpace--; }
}
