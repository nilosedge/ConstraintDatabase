package edu.southern.ConstraintDatabase;

import edu.southern.ConstraintDatabase.Gui.MainFrame;
import edu.southern.ConstraintDatabase.Logic.LogicHandler;


public class Main {
	public static void main( String[] args ) {
		LogicHandler logic = new LogicHandler();
		
		//CommandConsole console = new CommandConsole(logic);
		
		MainFrame frame = new MainFrame(logic);
		frame.setVisible(true);
		
		//GraphDisplay gc = new GraphDisplay("/Users/oblodgett/Desktop/Untitled.png");
		//gc.pack();
		//gc.setVisible(true);
		
	}
}
