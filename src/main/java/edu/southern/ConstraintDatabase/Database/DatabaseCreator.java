package edu.southern.ConstraintDatabase.Database;

import java.io.File;

import edu.southern.ConstraintDatabase.Database.dao.ColumnDAO;
import edu.southern.ConstraintDatabase.Database.dao.ColumnMappingDAO;
import edu.southern.ConstraintDatabase.Database.dao.ConstraintDAO;
import edu.southern.ConstraintDatabase.Database.dao.RowDAO;
import edu.southern.ConstraintDatabase.Database.dao.TableDAO;
import edu.southern.ConstraintDatabase.Database.dao.TableReferenceDAO;
import edu.southern.ConstraintDatabase.Database.util.SQLiteConnection;

public class DatabaseCreator {

	private SQLiteConnection con;
	
	protected TableDAO tableDAO;
	protected RowDAO rowDAO;
	protected ConstraintDAO constraintDAO;
	protected ColumnDAO columnDAO;
	protected TableReferenceDAO tableRefDAO;
	protected ColumnMappingDAO columnMapDAO;
	
	public DatabaseCreator(String filename, boolean debug) {
		con = new SQLiteConnection(debug);
		switchDatabase(filename);
	}
	
	public void switchDatabase(String filename) {
		if(new File(filename).exists()) {
			openDatabase(filename);
		} else {
			createDatabase(filename);
		}
	}
	
	public boolean deleteDatabase(String filename) {
		File file = new File(filename);
		return file.delete();
	}
	
	private void openDatabase(String filename) {
		con.setFilename(filename);
		setupDAOs();
	}
	
	private void createDatabase(String filename) {
		con.setFilename(filename);
		con.createTables();
		setupDAOs();
	}
	
	private void setupDAOs() {
		tableDAO = new TableDAO(con);
		tableRefDAO = new TableReferenceDAO(con);
		rowDAO = new RowDAO(con);
		constraintDAO = new ConstraintDAO(con);
		columnDAO = new ColumnDAO(con);
		columnMapDAO = new ColumnMappingDAO(con);
	}
}
