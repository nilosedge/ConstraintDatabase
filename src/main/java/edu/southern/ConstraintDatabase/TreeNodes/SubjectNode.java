package edu.southern.ConstraintDatabase.TreeNodes;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;

public class SubjectNode extends TreeNode {

	public DBInfoNode dbinfo;
	
	public SubjectNode() {
	}
	
	public SubjectNode(DBInfoNode dbinfo) {
		this.dbinfo = dbinfo;
	}

	public void Accept(TreeNodeVisitor visitor) throws TreeNodeException {
		visitor.Visit(this);
	}
	
}
