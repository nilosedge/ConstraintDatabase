package edu.southern.ConstraintDatabase.Database.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import edu.southern.ConstraintDatabase.Database.util.SQLiteConnection;

public class SQLiteDAO {

	private SQLiteConnection con;
	protected String sqlTableName;
	
	public SQLiteDAO(SQLiteConnection con) {
		this.con = con;
	}
	
	protected ResultSet runQuery(String query) {
		Statement statement = con.openStatement();
		if(con.debug) System.out.println("Query: " + query);
		try {
			statement.execute(query);
			return statement.getResultSet();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	protected void runUpdateQuery(String query) {
		Statement statement = con.openStatement();
		if(con.debug) System.out.println("Query: " + query);
		try {
			statement.executeUpdate(query);
			con.closeConnection(statement);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	protected ResultSet runQueryWithParam(String query, String value) {
		PreparedStatement statement = con.openPreparedStatement(query);
		try {
			statement.setString(1, value);
			statement.execute(query);
			return statement.getResultSet();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	protected void runUpdateQueryWithParam(String query, String value) {
		PreparedStatement statement = con.openPreparedStatement(query);
		try {
			statement.setString(1, value);
			statement.executeUpdate();
			con.closeConnection(statement);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	protected void closeResultSet(ResultSet res) {
		con.closeConnection(res);
	}
	
	protected ResultSet getRecordById(long id) {
		return runQuery("select * from " + sqlTableName + " where id = " + id);
	}
	
	protected ResultSet getRecordByName(String name) {
		return runQuery("select * from " + sqlTableName + " where name = '" + name + "'");
	}
}
