package edu.southern.ConstraintDatabase.Visitors;

import java.io.PrintStream;

import edu.southern.ConstraintDatabase.Database.models.ColumnModel;
import edu.southern.ConstraintDatabase.Database.models.ColumnMappingModel;
import edu.southern.ConstraintDatabase.Database.models.ConstraintModel;
import edu.southern.ConstraintDatabase.Database.models.RowModel;
import edu.southern.ConstraintDatabase.Database.models.TableModel;
import edu.southern.ConstraintDatabase.Database.models.TableReferenceModel;
import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.DatabaseDAOVisitor;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;
import edu.southern.ConstraintDatabase.TreeNodes.AssignmentNode;
import edu.southern.ConstraintDatabase.TreeNodes.BoundNode;
import edu.southern.ConstraintDatabase.TreeNodes.ClauseNode;
import edu.southern.ConstraintDatabase.TreeNodes.ConstraintNode;
import edu.southern.ConstraintDatabase.TreeNodes.DBInfoNode;
import edu.southern.ConstraintDatabase.TreeNodes.ExpressionNode;
import edu.southern.ConstraintDatabase.TreeNodes.FactorNode;
import edu.southern.ConstraintDatabase.TreeNodes.PredicateNode;
import edu.southern.ConstraintDatabase.TreeNodes.StringNode;
import edu.southern.ConstraintDatabase.TreeNodes.SubjectNode;
import edu.southern.ConstraintDatabase.TreeNodes.TermNode;
import edu.southern.ConstraintDatabase.TreeNodes.VariableNode;

public class PrinterVisitor extends PrinterUtil implements TreeNodeVisitor, DatabaseDAOVisitor {

	public PrinterVisitor (PrintStream out) {
		super(out);
	}

	public void Visit(ClauseNode clauseNode) throws TreeNodeException {
		clauseNode.subject.Accept(this);
		if(clauseNode.predicate != null) {
			Print(" :- ");
			clauseNode.predicate.Accept(this);
		}
		PrintLine(".");
	}
	public void Visit (SubjectNode subjectNode) throws TreeNodeException {
		subjectNode.dbinfo.Accept(this);
	}
	public void Visit(PredicateNode predicateNode) throws TreeNodeException {
		for(DBInfoNode db: predicateNode.dbInfos) {
			db.Accept(this);
			if(predicateNode.dbInfos.indexOf(db) != predicateNode.dbInfos.size() - 1) {
				Print(",");
			}
		}
		if(predicateNode.dbInfos.size() > 0) Print(",");

		for(ConstraintNode con: predicateNode.constraintNodes) {
			con.Accept(this);
			if(predicateNode.constraintNodes.indexOf(con) != predicateNode.constraintNodes.size() - 1) {
				Print(",");
			}
		}
	}
	public void Visit (DBInfoNode dbinfoNode) throws TreeNodeException {
		dbinfoNode.varnamenode.Accept(this);
		Print("(");
		for(VariableNode v: dbinfoNode.variables) {
			v.Accept(this);
			if(dbinfoNode.variables.indexOf(v) != dbinfoNode.variables.size() - 1) {
				Print(",");
			}
		}
		Print(")");
	}
	public void Visit (BoundNode boundNode) throws TreeNodeException {
		boundNode.varNode.Accept(this);
	}
	public void Visit (ConstraintNode constraintNode) throws TreeNodeException {
		constraintNode.en.Accept(this);
		constraintNode.an.Accept(this);
		constraintNode.bn.Accept(this);
	}

	public void Visit (ExpressionNode expessionNode) throws TreeNodeException {
		int count = 0;
		for(TermNode t: expessionNode.terms) {
			if(t.neg) {
				Print("-");
			} else {
				if(count > 0) Print("+");
			}
			t.Accept(this);
			count++;
		}
	}

	public void Visit(TermNode termNode) throws TreeNodeException {
		int count = 0;
		for(FactorNode f: termNode.factors) {
			if(count > 0) {
				Print(termNode.opers.get(count - 1));
			}
			f.Accept(this);
			count++;
		}
	}
	public void Visit(VariableNode varNode) {
		if(varNode.neg) {
			Print("-" + varNode.varname);
		} else {
			Print(varNode.varname);
		}
	}
	public void Visit(AssignmentNode assignNode) {
		Print(assignNode.type.getOper());
	}
	public void Visit (StringNode stringNode) {
		Print(stringNode.str);
	}
	public void Visit(FactorNode factorNode) throws TreeNodeException {
		if(factorNode.e != null) {
			Print("(");
			factorNode.e.Accept(this);
			Print(")");
		}
		if(factorNode.v != null) factorNode.v.Accept(this);
	}

	public void Visit(TableModel table) {
		for(RowModel r: table.rows) {
			Print(table.name);
			Print("(");
			int count = 0;
			for(ColumnModel c: table.columns) {
				c.Accept(this);
				if(count < table.columns.size() - 1) Print(",");
				count++;
			}
			Print(") :- ");
			r.Accept(this);
			PrintLine(".");
		}
	}

	public void Visit(ColumnModel column) {
		Print(column.name);
	}

	public void Visit(ColumnMappingModel columnMapping) {
		Print(columnMapping.mappedName);
	}

	public void Visit(ConstraintModel constraint) {
		try {
			constraint.constraintNode.Accept(this);
		} catch (TreeNodeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void Visit(RowModel row) {
		int count = 0;
		for(TableReferenceModel trm: row.refs) {
			trm.Accept(this);
			if(count < row.refs.size() - 1) Print(",");
			count++;
		}
		if(row.refs.size() > 0 && row.constraints.size() > 0) Print(",");
		count = 0;
		for(ConstraintModel c: row.constraints) {
			c.Accept(this);
			if(count < row.constraints.size() - 1) Print(",");
			count++;
		}
	}

	public void Visit(TableReferenceModel tableReference) {
		Print(tableReference.tableRef.name);
		Print("(");
		int count = 0;
		for(ColumnMappingModel cmm: tableReference.columnmappings) {
			cmm.Accept(this);
			if(count < tableReference.columnmappings.size() - 1) Print(",");
			count++;
		}
		Print(")");
	}

}
