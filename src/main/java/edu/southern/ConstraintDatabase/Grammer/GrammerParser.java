// $ANTLR 3.5 /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g 2013-05-14 16:52:40

package edu.southern.ConstraintDatabase.Grammer;

import edu.southern.ConstraintDatabase.TreeNodes.*;
import edu.southern.ConstraintDatabase.enums.AssignmentType;
import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class GrammerParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "COMMA", "DIGIT", "DIV", "EQ", 
		"GEQ", "GT", "LEQ", "LPAREN", "LT", "MINUS", "MULT", "NEQ", "NEWLINE", 
		"PLUS", "QUOTED", "RPAREN", "VARNAME", "WS", "'.'", "':-'"
	};
	public static final int EOF=-1;
	public static final int T__22=22;
	public static final int T__23=23;
	public static final int COMMA=4;
	public static final int DIGIT=5;
	public static final int DIV=6;
	public static final int EQ=7;
	public static final int GEQ=8;
	public static final int GT=9;
	public static final int LEQ=10;
	public static final int LPAREN=11;
	public static final int LT=12;
	public static final int MINUS=13;
	public static final int MULT=14;
	public static final int NEQ=15;
	public static final int NEWLINE=16;
	public static final int PLUS=17;
	public static final int QUOTED=18;
	public static final int RPAREN=19;
	public static final int VARNAME=20;
	public static final int WS=21;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public GrammerParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public GrammerParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return GrammerParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g"; }


	@Override
	public void emitErrorMessage(String msg) {
		//super.emitErrorMessage(msg);
	}

	@Override
	public void displayRecognitionError(String[] tokenNames, RecognitionException e) {
		super.displayRecognitionError(tokenNames, e);
		String hdr = getErrorHeader(e);
		String msg = getErrorMessage(e, tokenNames);
		String errorMessage = "";
		for (int i = 0; i < e.charPositionInLine; i++) errorMessage += " ";
		errorMessage += "^";
		System.out.println(errorMessage);
		System.out.println("Parser: An error occured on: " + hdr + " " + msg + "");
	}



	// $ANTLR start "clause"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:77:8: public clause returns [ClauseNode clause] : (s= subject ':-' p= predicate '.' ( NEWLINE )? |s= subject '.' ( NEWLINE )? );
	public final ClauseNode clause() throws RecognitionException {
		ClauseNode clause = null;


		SubjectNode s =null;
		PredicateNode p =null;

		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:77:41: (s= subject ':-' p= predicate '.' ( NEWLINE )? |s= subject '.' ( NEWLINE )? )
			int alt3=2;
			alt3 = dfa3.predict(input);
			switch (alt3) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:78:4: s= subject ':-' p= predicate '.' ( NEWLINE )?
					{
					pushFollow(FOLLOW_subject_in_clause220);
					s=subject();
					state._fsp--;

					match(input,23,FOLLOW_23_in_clause222); 
					pushFollow(FOLLOW_predicate_in_clause226);
					p=predicate();
					state._fsp--;

					match(input,22,FOLLOW_22_in_clause228); 
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:78:35: ( NEWLINE )?
					int alt1=2;
					int LA1_0 = input.LA(1);
					if ( (LA1_0==NEWLINE) ) {
						alt1=1;
					}
					switch (alt1) {
						case 1 :
							// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:78:35: NEWLINE
							{
							match(input,NEWLINE,FOLLOW_NEWLINE_in_clause230); 
							}
							break;

					}

					 clause = new ClauseNode(s, p); 
					}
					break;
				case 2 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:79:4: s= subject '.' ( NEWLINE )?
					{
					pushFollow(FOLLOW_subject_in_clause242);
					s=subject();
					state._fsp--;

					match(input,22,FOLLOW_22_in_clause244); 
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:79:18: ( NEWLINE )?
					int alt2=2;
					int LA2_0 = input.LA(1);
					if ( (LA2_0==NEWLINE) ) {
						alt2=1;
					}
					switch (alt2) {
						case 1 :
							// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:79:18: NEWLINE
							{
							match(input,NEWLINE,FOLLOW_NEWLINE_in_clause246); 
							}
							break;

					}

					 clause = new ClauseNode(s, null); 
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return clause;
	}
	// $ANTLR end "clause"



	// $ANTLR start "subject"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:82:1: subject returns [SubjectNode subject] : db= dbinfo ;
	public final SubjectNode subject() throws RecognitionException {
		SubjectNode subject = null;


		DBInfoNode db =null;

		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:82:37: (db= dbinfo )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:83:2: db= dbinfo
			{
			pushFollow(FOLLOW_dbinfo_in_subject266);
			db=dbinfo();
			state._fsp--;

			 return new SubjectNode(db); 
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return subject;
	}
	// $ANTLR end "subject"



	// $ANTLR start "predicate"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:86:1: predicate returns [PredicateNode predicate] : n1= node[predicate] ( COMMA n2= node[predicate] )* ;
	public final PredicateNode predicate() throws RecognitionException {
		PredicateNode predicate = null;


		 predicate = new PredicateNode(); 
		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:86:86: (n1= node[predicate] ( COMMA n2= node[predicate] )* )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:87:2: n1= node[predicate] ( COMMA n2= node[predicate] )*
			{
			pushFollow(FOLLOW_node_in_predicate288);
			node(predicate);
			state._fsp--;

			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:87:21: ( COMMA n2= node[predicate] )*
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( (LA4_0==COMMA) ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:87:22: COMMA n2= node[predicate]
					{
					match(input,COMMA,FOLLOW_COMMA_in_predicate292); 
					pushFollow(FOLLOW_node_in_predicate296);
					node(predicate);
					state._fsp--;

					}
					break;

				default :
					break loop4;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return predicate;
	}
	// $ANTLR end "predicate"



	// $ANTLR start "node"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:90:1: node[PredicateNode p] : (db= dbinfo |co= constraint );
	public final void node(PredicateNode p) throws RecognitionException {
		DBInfoNode db =null;
		ConstraintNode co =null;

		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:90:23: (db= dbinfo |co= constraint )
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0==VARNAME) ) {
				int LA5_1 = input.LA(2);
				if ( (LA5_1==LPAREN) ) {
					alt5=1;
				}
				else if ( ((LA5_1 >= DIV && LA5_1 <= LEQ)||(LA5_1 >= LT && LA5_1 <= NEQ)||LA5_1==PLUS) ) {
					alt5=2;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 5, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

			}
			else if ( (LA5_0==DIGIT||LA5_0==LPAREN||LA5_0==MINUS||LA5_0==22) ) {
				alt5=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 5, 0, input);
				throw nvae;
			}

			switch (alt5) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:91:4: db= dbinfo
					{
					pushFollow(FOLLOW_dbinfo_in_node315);
					db=dbinfo();
					state._fsp--;

					 p.addDBInfo(db); 
					}
					break;
				case 2 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:92:4: co= constraint
					{
					pushFollow(FOLLOW_constraint_in_node324);
					co=constraint();
					state._fsp--;

					 p.addConstraint(co); 
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "node"



	// $ANTLR start "dbinfo"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:95:1: dbinfo returns [DBInfoNode dbinfo] : vn= variable_name LPAREN vl= var_list RPAREN ;
	public final DBInfoNode dbinfo() throws RecognitionException {
		DBInfoNode dbinfo = null;


		VariableNode vn =null;
		List<VariableNode> vl =null;

		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:95:34: (vn= variable_name LPAREN vl= var_list RPAREN )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:96:4: vn= variable_name LPAREN vl= var_list RPAREN
			{
			pushFollow(FOLLOW_variable_name_in_dbinfo343);
			vn=variable_name();
			state._fsp--;

			match(input,LPAREN,FOLLOW_LPAREN_in_dbinfo345); 
			pushFollow(FOLLOW_var_list_in_dbinfo349);
			vl=var_list();
			state._fsp--;

			match(input,RPAREN,FOLLOW_RPAREN_in_dbinfo351); 
			 return new DBInfoNode(vn, vl); 
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return dbinfo;
	}
	// $ANTLR end "dbinfo"



	// $ANTLR start "var_list"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:101:1: var_list returns [List<VariableNode> list] : v1= variable ( COMMA v2= variable )* ;
	public final List<VariableNode> var_list() throws RecognitionException {
		List<VariableNode> list = null;


		VariableNode v1 =null;
		VariableNode v2 =null;

		 list = new ArrayList<VariableNode>(); 
		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:101:90: (v1= variable ( COMMA v2= variable )* )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:103:4: v1= variable ( COMMA v2= variable )*
			{
			pushFollow(FOLLOW_variable_in_var_list383);
			v1=variable();
			state._fsp--;

			 list.add(v1); 
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:104:3: ( COMMA v2= variable )*
			loop6:
			while (true) {
				int alt6=2;
				int LA6_0 = input.LA(1);
				if ( (LA6_0==COMMA) ) {
					alt6=1;
				}

				switch (alt6) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:105:4: COMMA v2= variable
					{
					match(input,COMMA,FOLLOW_COMMA_in_var_list394); 
					pushFollow(FOLLOW_variable_in_var_list398);
					v2=variable();
					state._fsp--;

					 list.add(v2); 
					}
					break;

				default :
					break loop6;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return list;
	}
	// $ANTLR end "var_list"



	// $ANTLR start "variable"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:109:1: variable returns [VariableNode variable] : (vname= variable_name |vnum= variable_number );
	public final VariableNode variable() throws RecognitionException {
		VariableNode variable = null;


		VariableNode vname =null;
		VariableNode vnum =null;

		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:109:40: (vname= variable_name |vnum= variable_number )
			int alt7=2;
			int LA7_0 = input.LA(1);
			if ( (LA7_0==VARNAME) ) {
				alt7=1;
			}
			else if ( (LA7_0==DIGIT||LA7_0==22) ) {
				alt7=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 7, 0, input);
				throw nvae;
			}

			switch (alt7) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:110:2: vname= variable_name
					{
					pushFollow(FOLLOW_variable_name_in_variable421);
					vname=variable_name();
					state._fsp--;

					 variable = vname; 
					}
					break;
				case 2 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:111:4: vnum= variable_number
					{
					pushFollow(FOLLOW_variable_number_in_variable430);
					vnum=variable_number();
					state._fsp--;

					 variable = vnum; 
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return variable;
	}
	// $ANTLR end "variable"



	// $ANTLR start "constraint"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:114:1: constraint returns [ConstraintNode constraint] : e= expr a= assign b= bound ;
	public final ConstraintNode constraint() throws RecognitionException {
		ConstraintNode constraint = null;


		ExpressionNode e =null;
		AssignmentNode a =null;
		BoundNode b =null;

		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:114:46: (e= expr a= assign b= bound )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:115:4: e= expr a= assign b= bound
			{
			pushFollow(FOLLOW_expr_in_constraint449);
			e=expr();
			state._fsp--;

			pushFollow(FOLLOW_assign_in_constraint453);
			a=assign();
			state._fsp--;

			pushFollow(FOLLOW_bound_in_constraint457);
			b=bound();
			state._fsp--;

			 return new ConstraintNode(e, a, b); 
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return constraint;
	}
	// $ANTLR end "constraint"



	// $ANTLR start "assign"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:118:1: assign returns [AssignmentNode assign] : ( GT | LT | GEQ | LEQ | EQ | NEQ );
	public final AssignmentNode assign() throws RecognitionException {
		AssignmentNode assign = null;


		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:118:38: ( GT | LT | GEQ | LEQ | EQ | NEQ )
			int alt8=6;
			switch ( input.LA(1) ) {
			case GT:
				{
				alt8=1;
				}
				break;
			case LT:
				{
				alt8=2;
				}
				break;
			case GEQ:
				{
				alt8=3;
				}
				break;
			case LEQ:
				{
				alt8=4;
				}
				break;
			case EQ:
				{
				alt8=5;
				}
				break;
			case NEQ:
				{
				alt8=6;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 8, 0, input);
				throw nvae;
			}
			switch (alt8) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:119:2: GT
					{
					match(input,GT,FOLLOW_GT_in_assign472); 
					 assign = new AssignmentNode(AssignmentType.GreaterThan); 
					}
					break;
				case 2 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:120:4: LT
					{
					match(input,LT,FOLLOW_LT_in_assign479); 
					 assign = new AssignmentNode(AssignmentType.LessThan); 
					}
					break;
				case 3 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:121:4: GEQ
					{
					match(input,GEQ,FOLLOW_GEQ_in_assign486); 
					 assign = new AssignmentNode(AssignmentType.GreaterThanOrEqualTo); 
					}
					break;
				case 4 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:122:4: LEQ
					{
					match(input,LEQ,FOLLOW_LEQ_in_assign493); 
					 assign = new AssignmentNode(AssignmentType.LessThanOrEqualTo); 
					}
					break;
				case 5 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:123:4: EQ
					{
					match(input,EQ,FOLLOW_EQ_in_assign500); 
					 assign = new AssignmentNode(AssignmentType.EqualTo); 
					}
					break;
				case 6 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:124:4: NEQ
					{
					match(input,NEQ,FOLLOW_NEQ_in_assign507); 
					 assign = new AssignmentNode(AssignmentType.NotEqualTo); 
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return assign;
	}
	// $ANTLR end "assign"



	// $ANTLR start "expr"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:128:1: expr returns [ExpressionNode expr] : t1= term ( ( PLUS | MINUS ) t2= term )* ;
	public final ExpressionNode expr() throws RecognitionException {
		ExpressionNode expr = null;


		TermNode t1 =null;
		TermNode t2 =null;

		 expr = new ExpressionNode(); boolean neg=false; 
		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:128:92: (t1= term ( ( PLUS | MINUS ) t2= term )* )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:129:2: t1= term ( ( PLUS | MINUS ) t2= term )*
			{
			pushFollow(FOLLOW_term_in_expr530);
			t1=term();
			state._fsp--;

			 expr.addTermNode(t1); 
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:130:3: ( ( PLUS | MINUS ) t2= term )*
			loop10:
			while (true) {
				int alt10=2;
				int LA10_0 = input.LA(1);
				if ( (LA10_0==MINUS||LA10_0==PLUS) ) {
					alt10=1;
				}

				switch (alt10) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:131:4: ( PLUS | MINUS ) t2= term
					{
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:131:4: ( PLUS | MINUS )
					int alt9=2;
					int LA9_0 = input.LA(1);
					if ( (LA9_0==PLUS) ) {
						alt9=1;
					}
					else if ( (LA9_0==MINUS) ) {
						alt9=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 9, 0, input);
						throw nvae;
					}

					switch (alt9) {
						case 1 :
							// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:131:5: PLUS
							{
							match(input,PLUS,FOLLOW_PLUS_in_expr542); 
							}
							break;
						case 2 :
							// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:131:12: MINUS
							{
							match(input,MINUS,FOLLOW_MINUS_in_expr546); 
							 neg=true; 
							}
							break;

					}

					pushFollow(FOLLOW_term_in_expr553);
					t2=term();
					state._fsp--;

					 t2.neg = neg; expr.addTermNode(t2); 
					}
					break;

				default :
					break loop10;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return expr;
	}
	// $ANTLR end "expr"



	// $ANTLR start "term"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:135:1: term returns [TermNode term] : f1= factor ( ( MULT | DIV ) f2= factor )* ;
	public final TermNode term() throws RecognitionException {
		TermNode term = null;


		FactorNode f1 =null;
		FactorNode f2 =null;

		 term = new TermNode(); boolean mult=false; 
		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:135:81: (f1= factor ( ( MULT | DIV ) f2= factor )* )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:136:2: f1= factor ( ( MULT | DIV ) f2= factor )*
			{
			pushFollow(FOLLOW_factor_in_term581);
			f1=factor();
			state._fsp--;

			term.addFirstFactorNode(f1); 
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:137:3: ( ( MULT | DIV ) f2= factor )*
			loop12:
			while (true) {
				int alt12=2;
				int LA12_0 = input.LA(1);
				if ( (LA12_0==DIV||LA12_0==MULT) ) {
					alt12=1;
				}

				switch (alt12) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:138:4: ( MULT | DIV ) f2= factor
					{
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:138:4: ( MULT | DIV )
					int alt11=2;
					int LA11_0 = input.LA(1);
					if ( (LA11_0==MULT) ) {
						alt11=1;
					}
					else if ( (LA11_0==DIV) ) {
						alt11=2;
					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 11, 0, input);
						throw nvae;
					}

					switch (alt11) {
						case 1 :
							// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:138:5: MULT
							{
							match(input,MULT,FOLLOW_MULT_in_term593); 
							 mult=true; 
							}
							break;
						case 2 :
							// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:138:27: DIV
							{
							match(input,DIV,FOLLOW_DIV_in_term599); 
							}
							break;

					}

					pushFollow(FOLLOW_factor_in_term604);
					f2=factor();
					state._fsp--;

					 if(mult) term.addFactorNode(f2, "*"); else term.addFactorNode(f2, "/"); 
					}
					break;

				default :
					break loop12;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return term;
	}
	// $ANTLR end "term"



	// $ANTLR start "factor"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:142:1: factor returns [FactorNode factor] : ( MINUS v1= variable_name |v2= variable_name | MINUS v3= variable_number |v4= variable_number | LPAREN e= expr RPAREN );
	public final FactorNode factor() throws RecognitionException {
		FactorNode factor = null;


		VariableNode v1 =null;
		VariableNode v2 =null;
		VariableNode v3 =null;
		VariableNode v4 =null;
		ExpressionNode e =null;

		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:142:34: ( MINUS v1= variable_name |v2= variable_name | MINUS v3= variable_number |v4= variable_number | LPAREN e= expr RPAREN )
			int alt13=5;
			switch ( input.LA(1) ) {
			case MINUS:
				{
				int LA13_1 = input.LA(2);
				if ( (LA13_1==VARNAME) ) {
					alt13=1;
				}
				else if ( (LA13_1==DIGIT||LA13_1==22) ) {
					alt13=3;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 13, 1, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case VARNAME:
				{
				alt13=2;
				}
				break;
			case DIGIT:
			case 22:
				{
				alt13=4;
				}
				break;
			case LPAREN:
				{
				alt13=5;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 13, 0, input);
				throw nvae;
			}
			switch (alt13) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:143:2: MINUS v1= variable_name
					{
					match(input,MINUS,FOLLOW_MINUS_in_factor624); 
					pushFollow(FOLLOW_variable_name_in_factor628);
					v1=variable_name();
					state._fsp--;

					 v1.neg = true; factor = new FactorNode(v1); 
					}
					break;
				case 2 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:144:4: v2= variable_name
					{
					pushFollow(FOLLOW_variable_name_in_factor637);
					v2=variable_name();
					state._fsp--;

					 factor = new FactorNode(v2); 
					}
					break;
				case 3 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:145:4: MINUS v3= variable_number
					{
					match(input,MINUS,FOLLOW_MINUS_in_factor644); 
					pushFollow(FOLLOW_variable_number_in_factor648);
					v3=variable_number();
					state._fsp--;

					 v3.neg = true; factor = new FactorNode(v3); 
					}
					break;
				case 4 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:146:4: v4= variable_number
					{
					pushFollow(FOLLOW_variable_number_in_factor657);
					v4=variable_number();
					state._fsp--;

					 factor = new FactorNode(v4); 
					}
					break;
				case 5 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:147:4: LPAREN e= expr RPAREN
					{
					match(input,LPAREN,FOLLOW_LPAREN_in_factor664); 
					pushFollow(FOLLOW_expr_in_factor668);
					e=expr();
					state._fsp--;

					match(input,RPAREN,FOLLOW_RPAREN_in_factor670); 
					 factor = new FactorNode(e); 
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return factor;
	}
	// $ANTLR end "factor"



	// $ANTLR start "bound"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:150:1: bound returns [BoundNode bound] : ( ( MINUS )? n1= variable_number |n2= string );
	public final BoundNode bound() throws RecognitionException {
		BoundNode bound = null;


		VariableNode n1 =null;
		VariableNode n2 =null;

		 boolean neg=false; 
		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:150:60: ( ( MINUS )? n1= variable_number |n2= string )
			int alt15=2;
			int LA15_0 = input.LA(1);
			if ( (LA15_0==DIGIT||LA15_0==MINUS||LA15_0==22) ) {
				alt15=1;
			}
			else if ( (LA15_0==QUOTED) ) {
				alt15=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 15, 0, input);
				throw nvae;
			}

			switch (alt15) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:151:2: ( MINUS )? n1= variable_number
					{
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:151:2: ( MINUS )?
					int alt14=2;
					int LA14_0 = input.LA(1);
					if ( (LA14_0==MINUS) ) {
						alt14=1;
					}
					switch (alt14) {
						case 1 :
							// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:151:3: MINUS
							{
							match(input,MINUS,FOLLOW_MINUS_in_bound691); 
							 neg=true; 
							}
							break;

					}

					pushFollow(FOLLOW_variable_number_in_bound699);
					n1=variable_number();
					state._fsp--;

					 n1.neg=neg; bound = new BoundNode(n1); 
					}
					break;
				case 2 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:152:4: n2= string
					{
					pushFollow(FOLLOW_string_in_bound708);
					n2=string();
					state._fsp--;

					 n2.sb = true; bound = new BoundNode(n2); 
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return bound;
	}
	// $ANTLR end "bound"



	// $ANTLR start "variable_number"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:155:1: variable_number returns [VariableNode variable_number] : num= number ;
	public final VariableNode variable_number() throws RecognitionException {
		VariableNode variable_number = null;


		ParserRuleReturnScope num =null;

		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:155:54: (num= number )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:156:2: num= number
			{
			pushFollow(FOLLOW_number_in_variable_number725);
			num=number();
			state._fsp--;

			 variable_number = new VariableNode((num!=null?input.toString(num.start,num.stop):null), true); 
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return variable_number;
	}
	// $ANTLR end "variable_number"



	// $ANTLR start "variable_name"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:158:1: variable_name returns [VariableNode variable_name] : name= VARNAME ;
	public final VariableNode variable_name() throws RecognitionException {
		VariableNode variable_name = null;


		Token name=null;

		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:158:50: (name= VARNAME )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:159:2: name= VARNAME
			{
			name=(Token)match(input,VARNAME,FOLLOW_VARNAME_in_variable_name740); 
			 variable_name = new VariableNode((name!=null?name.getText():null), false); 
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return variable_name;
	}
	// $ANTLR end "variable_name"


	public static class number_return extends ParserRuleReturnScope {
	};


	// $ANTLR start "number"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:161:1: number : ( ( DIGIT )+ '.' ( DIGIT )+ | ( '.' )? ( DIGIT )+ );
	public final GrammerParser.number_return number() throws RecognitionException {
		GrammerParser.number_return retval = new GrammerParser.number_return();
		retval.start = input.LT(1);

		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:161:7: ( ( DIGIT )+ '.' ( DIGIT )+ | ( '.' )? ( DIGIT )+ )
			int alt20=2;
			alt20 = dfa20.predict(input);
			switch (alt20) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:162:2: ( DIGIT )+ '.' ( DIGIT )+
					{
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:162:2: ( DIGIT )+
					int cnt16=0;
					loop16:
					while (true) {
						int alt16=2;
						int LA16_0 = input.LA(1);
						if ( (LA16_0==DIGIT) ) {
							alt16=1;
						}

						switch (alt16) {
						case 1 :
							// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:162:2: DIGIT
							{
							match(input,DIGIT,FOLLOW_DIGIT_in_number753); 
							}
							break;

						default :
							if ( cnt16 >= 1 ) break loop16;
							EarlyExitException eee = new EarlyExitException(16, input);
							throw eee;
						}
						cnt16++;
					}

					match(input,22,FOLLOW_22_in_number756); 
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:162:13: ( DIGIT )+
					int cnt17=0;
					loop17:
					while (true) {
						int alt17=2;
						int LA17_0 = input.LA(1);
						if ( (LA17_0==DIGIT) ) {
							alt17=1;
						}

						switch (alt17) {
						case 1 :
							// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:162:13: DIGIT
							{
							match(input,DIGIT,FOLLOW_DIGIT_in_number758); 
							}
							break;

						default :
							if ( cnt17 >= 1 ) break loop17;
							EarlyExitException eee = new EarlyExitException(17, input);
							throw eee;
						}
						cnt17++;
					}

					}
					break;
				case 2 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:162:22: ( '.' )? ( DIGIT )+
					{
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:162:22: ( '.' )?
					int alt18=2;
					int LA18_0 = input.LA(1);
					if ( (LA18_0==22) ) {
						alt18=1;
					}
					switch (alt18) {
						case 1 :
							// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:162:22: '.'
							{
							match(input,22,FOLLOW_22_in_number763); 
							}
							break;

					}

					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:162:27: ( DIGIT )+
					int cnt19=0;
					loop19:
					while (true) {
						int alt19=2;
						int LA19_0 = input.LA(1);
						if ( (LA19_0==DIGIT) ) {
							alt19=1;
						}

						switch (alt19) {
						case 1 :
							// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:162:27: DIGIT
							{
							match(input,DIGIT,FOLLOW_DIGIT_in_number766); 
							}
							break;

						default :
							if ( cnt19 >= 1 ) break loop19;
							EarlyExitException eee = new EarlyExitException(19, input);
							throw eee;
						}
						cnt19++;
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "number"



	// $ANTLR start "string"
	// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:174:1: string returns [VariableNode variable_string] : q= QUOTED ;
	public final VariableNode string() throws RecognitionException {
		VariableNode variable_string = null;


		Token q=null;

		try {
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:174:45: (q= QUOTED )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:175:2: q= QUOTED
			{
			q=(Token)match(input,QUOTED,FOLLOW_QUOTED_in_string851); 
			 return new VariableNode((q!=null?q.getText():null), false); 
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return variable_string;
	}
	// $ANTLR end "string"

	// Delegated rules


	protected DFA3 dfa3 = new DFA3(this);
	protected DFA20 dfa20 = new DFA20(this);
	static final String DFA3_eotS =
		"\23\uffff";
	static final String DFA3_eofS =
		"\23\uffff";
	static final String DFA3_minS =
		"\1\24\1\13\1\5\2\4\2\5\1\26\1\5\3\4\1\5\2\uffff\1\4\1\5\2\4";
	static final String DFA3_maxS =
		"\1\24\1\13\1\26\1\23\1\26\1\5\1\26\1\27\1\5\2\23\1\26\1\5\2\uffff\1\23"+
		"\1\5\2\23";
	static final String DFA3_acceptS =
		"\15\uffff\1\1\1\2\4\uffff";
	static final String DFA3_specialS =
		"\23\uffff}>";
	static final String[] DFA3_transitionS = {
			"\1\1",
			"\1\2",
			"\1\4\16\uffff\1\3\1\uffff\1\5",
			"\1\6\16\uffff\1\7",
			"\1\6\1\4\15\uffff\1\7\2\uffff\1\10",
			"\1\11",
			"\1\13\16\uffff\1\12\1\uffff\1\14",
			"\1\16\1\15",
			"\1\17",
			"\1\6\1\11\15\uffff\1\7",
			"\1\6\16\uffff\1\7",
			"\1\6\1\13\15\uffff\1\7\2\uffff\1\20",
			"\1\21",
			"",
			"",
			"\1\6\1\17\15\uffff\1\7",
			"\1\22",
			"\1\6\1\21\15\uffff\1\7",
			"\1\6\1\22\15\uffff\1\7"
	};

	static final short[] DFA3_eot = DFA.unpackEncodedString(DFA3_eotS);
	static final short[] DFA3_eof = DFA.unpackEncodedString(DFA3_eofS);
	static final char[] DFA3_min = DFA.unpackEncodedStringToUnsignedChars(DFA3_minS);
	static final char[] DFA3_max = DFA.unpackEncodedStringToUnsignedChars(DFA3_maxS);
	static final short[] DFA3_accept = DFA.unpackEncodedString(DFA3_acceptS);
	static final short[] DFA3_special = DFA.unpackEncodedString(DFA3_specialS);
	static final short[][] DFA3_transition;

	static {
		int numStates = DFA3_transitionS.length;
		DFA3_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA3_transition[i] = DFA.unpackEncodedString(DFA3_transitionS[i]);
		}
	}

	protected class DFA3 extends DFA {

		public DFA3(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 3;
			this.eot = DFA3_eot;
			this.eof = DFA3_eof;
			this.min = DFA3_min;
			this.max = DFA3_max;
			this.accept = DFA3_accept;
			this.special = DFA3_special;
			this.transition = DFA3_transition;
		}
		@Override
		public String getDescription() {
			return "77:8: public clause returns [ClauseNode clause] : (s= subject ':-' p= predicate '.' ( NEWLINE )? |s= subject '.' ( NEWLINE )? );";
		}
	}

	static final String DFA20_eotS =
		"\5\uffff";
	static final String DFA20_eofS =
		"\3\uffff\1\2\1\uffff";
	static final String DFA20_minS =
		"\1\5\1\4\1\uffff\1\5\1\uffff";
	static final String DFA20_maxS =
		"\2\26\1\uffff\1\20\1\uffff";
	static final String DFA20_acceptS =
		"\2\uffff\1\2\1\uffff\1\1";
	static final String DFA20_specialS =
		"\5\uffff}>";
	static final String[] DFA20_transitionS = {
			"\1\1\20\uffff\1\2",
			"\1\2\1\1\5\2\1\uffff\4\2\1\uffff\1\2\1\uffff\1\2\2\uffff\1\3",
			"",
			"\1\4\12\uffff\1\2",
			""
	};

	static final short[] DFA20_eot = DFA.unpackEncodedString(DFA20_eotS);
	static final short[] DFA20_eof = DFA.unpackEncodedString(DFA20_eofS);
	static final char[] DFA20_min = DFA.unpackEncodedStringToUnsignedChars(DFA20_minS);
	static final char[] DFA20_max = DFA.unpackEncodedStringToUnsignedChars(DFA20_maxS);
	static final short[] DFA20_accept = DFA.unpackEncodedString(DFA20_acceptS);
	static final short[] DFA20_special = DFA.unpackEncodedString(DFA20_specialS);
	static final short[][] DFA20_transition;

	static {
		int numStates = DFA20_transitionS.length;
		DFA20_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA20_transition[i] = DFA.unpackEncodedString(DFA20_transitionS[i]);
		}
	}

	protected class DFA20 extends DFA {

		public DFA20(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 20;
			this.eot = DFA20_eot;
			this.eof = DFA20_eof;
			this.min = DFA20_min;
			this.max = DFA20_max;
			this.accept = DFA20_accept;
			this.special = DFA20_special;
			this.transition = DFA20_transition;
		}
		@Override
		public String getDescription() {
			return "161:1: number : ( ( DIGIT )+ '.' ( DIGIT )+ | ( '.' )? ( DIGIT )+ );";
		}
	}

	public static final BitSet FOLLOW_subject_in_clause220 = new BitSet(new long[]{0x0000000000800000L});
	public static final BitSet FOLLOW_23_in_clause222 = new BitSet(new long[]{0x0000000000502820L});
	public static final BitSet FOLLOW_predicate_in_clause226 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_22_in_clause228 = new BitSet(new long[]{0x0000000000010002L});
	public static final BitSet FOLLOW_NEWLINE_in_clause230 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_subject_in_clause242 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_22_in_clause244 = new BitSet(new long[]{0x0000000000010002L});
	public static final BitSet FOLLOW_NEWLINE_in_clause246 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_dbinfo_in_subject266 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_node_in_predicate288 = new BitSet(new long[]{0x0000000000000012L});
	public static final BitSet FOLLOW_COMMA_in_predicate292 = new BitSet(new long[]{0x0000000000502820L});
	public static final BitSet FOLLOW_node_in_predicate296 = new BitSet(new long[]{0x0000000000000012L});
	public static final BitSet FOLLOW_dbinfo_in_node315 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_constraint_in_node324 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_variable_name_in_dbinfo343 = new BitSet(new long[]{0x0000000000000800L});
	public static final BitSet FOLLOW_LPAREN_in_dbinfo345 = new BitSet(new long[]{0x0000000000500020L});
	public static final BitSet FOLLOW_var_list_in_dbinfo349 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_RPAREN_in_dbinfo351 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_variable_in_var_list383 = new BitSet(new long[]{0x0000000000000012L});
	public static final BitSet FOLLOW_COMMA_in_var_list394 = new BitSet(new long[]{0x0000000000500020L});
	public static final BitSet FOLLOW_variable_in_var_list398 = new BitSet(new long[]{0x0000000000000012L});
	public static final BitSet FOLLOW_variable_name_in_variable421 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_variable_number_in_variable430 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr_in_constraint449 = new BitSet(new long[]{0x0000000000009780L});
	public static final BitSet FOLLOW_assign_in_constraint453 = new BitSet(new long[]{0x0000000000442020L});
	public static final BitSet FOLLOW_bound_in_constraint457 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GT_in_assign472 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LT_in_assign479 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_GEQ_in_assign486 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LEQ_in_assign493 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_EQ_in_assign500 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NEQ_in_assign507 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_term_in_expr530 = new BitSet(new long[]{0x0000000000022002L});
	public static final BitSet FOLLOW_PLUS_in_expr542 = new BitSet(new long[]{0x0000000000502820L});
	public static final BitSet FOLLOW_MINUS_in_expr546 = new BitSet(new long[]{0x0000000000502820L});
	public static final BitSet FOLLOW_term_in_expr553 = new BitSet(new long[]{0x0000000000022002L});
	public static final BitSet FOLLOW_factor_in_term581 = new BitSet(new long[]{0x0000000000004042L});
	public static final BitSet FOLLOW_MULT_in_term593 = new BitSet(new long[]{0x0000000000502820L});
	public static final BitSet FOLLOW_DIV_in_term599 = new BitSet(new long[]{0x0000000000502820L});
	public static final BitSet FOLLOW_factor_in_term604 = new BitSet(new long[]{0x0000000000004042L});
	public static final BitSet FOLLOW_MINUS_in_factor624 = new BitSet(new long[]{0x0000000000100000L});
	public static final BitSet FOLLOW_variable_name_in_factor628 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_variable_name_in_factor637 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_MINUS_in_factor644 = new BitSet(new long[]{0x0000000000400020L});
	public static final BitSet FOLLOW_variable_number_in_factor648 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_variable_number_in_factor657 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAREN_in_factor664 = new BitSet(new long[]{0x0000000000502820L});
	public static final BitSet FOLLOW_expr_in_factor668 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_RPAREN_in_factor670 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_MINUS_in_bound691 = new BitSet(new long[]{0x0000000000400020L});
	public static final BitSet FOLLOW_variable_number_in_bound699 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_string_in_bound708 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_number_in_variable_number725 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VARNAME_in_variable_name740 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_DIGIT_in_number753 = new BitSet(new long[]{0x0000000000400020L});
	public static final BitSet FOLLOW_22_in_number756 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_DIGIT_in_number758 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_22_in_number763 = new BitSet(new long[]{0x0000000000000020L});
	public static final BitSet FOLLOW_DIGIT_in_number766 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_QUOTED_in_string851 = new BitSet(new long[]{0x0000000000000002L});
}
