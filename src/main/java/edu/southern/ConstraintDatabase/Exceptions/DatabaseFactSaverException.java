package edu.southern.ConstraintDatabase.Exceptions;

public class DatabaseFactSaverException extends TreeNodeException {
	private static final long serialVersionUID = -6879461730711069575L;

	public DatabaseFactSaverException(String str) {
		super(str);
	}
}
