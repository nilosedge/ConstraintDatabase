package edu.southern.ConstraintDatabase.TreeNodes;

import java.util.List;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;

public class DBInfoNode extends TreeNode {
	
	public VariableNode varnamenode;
	public List<VariableNode> variables;
	
	public DBInfoNode() {
	}
	
	public DBInfoNode(VariableNode varnamenode, List<VariableNode> variables) {
		this.varnamenode = varnamenode;
		this.variables = variables;
	}

	@Override
	public void Accept(TreeNodeVisitor visitor) throws TreeNodeException {
		visitor.Visit(this);
	}
	
}
