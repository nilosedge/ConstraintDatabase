package edu.southern.ConstraintDatabase.Database.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.sqlite.JDBC;

public class SQLiteConnection {

	private String connectionString = null;
	private String sqlfilename = null;
	private Connection sharedConnection = null;
	public boolean debug = false;
	
	public SQLiteConnection(boolean debug) {
		sqlfilename = "Default.sqlite";
		connectionString = "jdbc:sqlite:" + sqlfilename;
		sharedConnection = null;
		this.debug = debug;
	}
	
	public void setFilename(String filename) {
		sqlfilename = filename;
		connectionString = "jdbc:sqlite:" + sqlfilename;
		sharedConnection = null;
	}
	
	private Connection getConnection() {
		try {
			if(sharedConnection == null) {
				Class.forName("org.sqlite.JDBC");
				sharedConnection = DriverManager.getConnection(connectionString);
			}
			return sharedConnection;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("No Driver Found for SQLite: " + e.getMessage());
			System.exit(0);
		}
		return null;
	}
	
	public void closeConnection(Statement statement) {
		try {
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void closeConnection(ResultSet res) {
		try {
			if(res != null) {
				Statement statement = res.getStatement();
				res.close();
				statement.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Statement openStatement() {
		try {
			getConnection();
			return sharedConnection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public PreparedStatement openPreparedStatement(String query) {
		try {
			getConnection();
			return sharedConnection.prepareStatement(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void createTables() {
		
		String tableCreate = "CREATE TABLE tables (id INTEGER PRIMARY KEY, name VARCHAR(255));";
		String tableCreateIndex = "CREATE INDEX table_name_idx ON tables(name)";
		String rowCreate = "CREATE TABLE rows (id INTEGER PRIMARY KEY, tableId INTEGER, FOREIGN KEY(tableId) REFERENCES tables(id));";
		String rowCreateIndex = "CREATE INDEX row_tableId_idx ON rows(tableId)";
		String constraintCreate = "CREATE TABLE constraints (id INTEGER PRIMARY KEY, rowId INTEGER, dataField BLOB, FOREIGN KEY(rowId) REFERENCES rows(id));";
		String constraintCreateIndex = "CREATE INDEX constraints_rowId_idx ON constraints(rowId)";
		String columnCreate = "CREATE TABLE columns (id INTEGER PRIMARY KEY, tableId INTEGER, name VARCHAR(255), FOREIGN KEY(tableId) REFERENCES tables(id));";
		String columnCreateIndex1 = "CREATE INDEX columns_tableId_idx ON columns(tableId)";
		String columnCreateIndex2 = "CREATE INDEX columns_name_idx ON columns(name)";
		String columnCreateIndex3 = "CREATE INDEX columns_tableId_name_idx ON columns(tableId, name)";
		String tablerefsCreate = "CREATE TABLE tablerefs (id INTEGER PRIMARY KEY, rowId INTEGER, tableId INTEGER, FOREIGN KEY(rowId) REFERENCES rows(id), FOREIGN KEY(tableId) REFERENCES tables(id));";
		String tablerefsCreate1 = "CREATE INDEX tablerefs_rowId_idx ON tablerefs(rowId)";
		String tablerefsCreate2 = "CREATE INDEX tablerefs_tableId_idx ON tablerefs(tableId)";
		String tablerefsCreate3 = "CREATE INDEX tablerefs_rowId_tableId_idx ON tablerefs(rowId, tableId)";
		String columnMappingCreate = "CREATE TABLE columnmappings (id INTEGER PRIMARY KEY, tableRefId INTEGER, columnId INTEGER, mappedName VARCHAR(255), FOREIGN KEY(tableRefId) REFERENCES tablerefs(id), FOREIGN KEY(columnId) REFERENCES columns(id));";
		String columnMappingCreate1 = "CREATE INDEX columnmappings_tableRefId_idx ON columnmappings(tableRefId)";
		String columnMappingCreate2 = "CREATE INDEX columnmappings_columnId_idx ON columnmappings(columnId)";
		String columnMappingCreate3 = "CREATE INDEX columnmappings_tableRefId_columnId_idx ON columnmappings(tableRefId, columnId)";
		
		
		try {
			System.out.println("Creating Tables for New Database: " + sqlfilename);
			getConnection();
			sharedConnection.createStatement().execute(tableCreate);
			sharedConnection.createStatement().execute(tableCreateIndex);
			sharedConnection.createStatement().execute(rowCreate);
			sharedConnection.createStatement().execute(rowCreateIndex);
			sharedConnection.createStatement().execute(constraintCreate);
			sharedConnection.createStatement().execute(constraintCreateIndex);
			sharedConnection.createStatement().execute(columnCreate);
			sharedConnection.createStatement().execute(columnCreateIndex1);
			sharedConnection.createStatement().execute(columnCreateIndex2);
			sharedConnection.createStatement().execute(columnCreateIndex3);
			sharedConnection.createStatement().execute(tablerefsCreate);
			sharedConnection.createStatement().execute(tablerefsCreate1);
			sharedConnection.createStatement().execute(tablerefsCreate2);
			sharedConnection.createStatement().execute(tablerefsCreate3);
			sharedConnection.createStatement().execute(columnMappingCreate);
			sharedConnection.createStatement().execute(columnMappingCreate1);
			sharedConnection.createStatement().execute(columnMappingCreate2);
			sharedConnection.createStatement().execute(columnMappingCreate3);
			//con.close();
			System.out.println("Finished Creating tables for: " + sqlfilename);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
}
