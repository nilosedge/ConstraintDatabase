package edu.southern.ConstraintDatabase.Interfaces;

import edu.southern.ConstraintDatabase.Database.models.ColumnModel;
import edu.southern.ConstraintDatabase.Database.models.ColumnMappingModel;
import edu.southern.ConstraintDatabase.Database.models.ConstraintModel;
import edu.southern.ConstraintDatabase.Database.models.RowModel;
import edu.southern.ConstraintDatabase.Database.models.TableModel;
import edu.southern.ConstraintDatabase.Database.models.TableReferenceModel;

public interface DatabaseDAOVisitor {
	void Visit (TableModel table);
	void Visit(ColumnModel column);
	void Visit(ColumnMappingModel columnMapping);
	void Visit(ConstraintModel constraint);
	void Visit(RowModel row);
	void Visit(TableReferenceModel tableReference);
}
