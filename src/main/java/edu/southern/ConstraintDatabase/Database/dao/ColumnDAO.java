package edu.southern.ConstraintDatabase.Database.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.southern.ConstraintDatabase.Database.models.ColumnModel;
import edu.southern.ConstraintDatabase.Database.util.SQLiteConnection;

public class ColumnDAO extends SQLiteDAO {
	
	public ColumnDAO(SQLiteConnection con) {
		super(con);
		sqlTableName = "columns";
	}
	
	public List<ColumnModel> getColumnsByTableId(long tableId) {
		List<ColumnModel> list = new ArrayList<ColumnModel>();
		ResultSet set = runQuery("select * from " + sqlTableName + " where tableId = " + tableId + " order by id");
		try {
			if(set.isBeforeFirst()) {
				while(set.next()) {
					list.add(populate(set));
				}
			}
			closeResultSet(set);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return list;
	}
	
	public ColumnModel getColumnByName(long tableId, String name) {
		ResultSet set = runQuery("select * from " + sqlTableName + " where tableId = " + tableId + " and name = '" + name + "'" + " order by id");
		ColumnModel c = null;
		try {
			if(set.isBeforeFirst()) {
				while(set.next()) {
					c = populate(set);
					break;
				}
			}
			closeResultSet(set);
		} catch (SQLException e) {
			c = null;
			e.printStackTrace();
		}
		return c;
	}
	
	public ColumnModel columnExists(long tableId, String name) {
		return getColumnByName(tableId, name);
	}
	
	public ColumnModel createColumn(long tableId, String name) {
		runUpdateQuery("insert into " + sqlTableName + " (tableId, name) values(" + tableId + ",'" + name + "')");
		return getColumnByName(tableId, name);
	}
	
	public void deleteColumnsByTableId(long tableId) {
		runUpdateQuery("delete from " + sqlTableName + " where tableId=" + tableId);
	}
	
	private ColumnModel populate(ResultSet set) {
		ColumnModel c = new ColumnModel();
		try {
			c.id = set.getLong(1);
			c.tableId = set.getLong(2);
			c.name = set.getString(3);
		} catch (SQLException e) {
			e.printStackTrace();
			c = null;
		}
		return c;
	}
	
	public ColumnModel getColumnById(long id) {
		ResultSet set = getRecordById(id);
		ColumnModel c = null;
		try {
			if(set.isBeforeFirst()) {
				while(set.next()) {
					c = populate(set);
					break;
				}
			}
			closeResultSet(set);
		} catch (SQLException e) {
			c = null;
			e.printStackTrace();
		}
		return c;
	}

}
