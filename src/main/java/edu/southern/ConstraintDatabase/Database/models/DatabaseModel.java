package edu.southern.ConstraintDatabase.Database.models;

import edu.southern.ConstraintDatabase.Interfaces.DatabaseDAOVisitor;

public abstract class DatabaseModel {
	public abstract void Accept(DatabaseDAOVisitor visitor);
	
	public DatabaseModel() {
	}
}
