package edu.southern.ConstraintDatabase.GraphUtils;

import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GraphDisplay extends JFrame {
	private static final long serialVersionUID = -3430382051964446974L;

	private String fileName;
	
	public GraphDisplay(String filename) {
		this.fileName = filename;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				new File(fileName).delete();
			}
		});
		
		ImageIcon icon = new ImageIcon(filename);
		setSize(icon.getIconWidth(), icon.getIconHeight());
		JPanel panel = new JPanel(); 
		panel.setSize(icon.getIconWidth(),icon.getIconHeight());
		panel.setBackground(Color.BLACK);
		JLabel label = new JLabel(); 
		label.setIcon(icon); 
		panel.add(label);
		add(panel);
		pack();
	}

}
