package edu.southern.ConstraintDatabase.TreeNodes;

import java.util.ArrayList;
import java.util.List;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;

public class TermNode extends TreeNode {

	public List<FactorNode> factors = new ArrayList<FactorNode>();
	public List<String> opers = new ArrayList<String>();
	public boolean neg = false;
	
	public TermNode() {
	}
	
	public void addFirstFactorNode(FactorNode first) {
		factors.add(first);
	}
	
	public void addFactorNode(FactorNode factor, String oper) {
		factors.add(factor);
		opers.add(oper);
	}

	@Override
	public void Accept(TreeNodeVisitor visitor) throws TreeNodeException {
		visitor.Visit(this);
	}
	
}
