package edu.southern.ConstraintDatabase.TreeNodes;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;

public class BoundNode extends TreeNode {

	public VariableNode varNode;
	
	public BoundNode() {
	}
	
	public BoundNode(VariableNode varNode) {
		this.varNode = varNode;
	}
	
	public void makeNegative() {
		if(!varNode.sb) {
			varNode.neg = !varNode.neg;
		}
	}
	
	@Override
	public void Accept(TreeNodeVisitor visitor) throws TreeNodeException {
		visitor.Visit(this);
	}

}
