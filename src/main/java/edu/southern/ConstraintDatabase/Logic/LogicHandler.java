package edu.southern.ConstraintDatabase.Logic;

import java.io.File;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import edu.southern.ConstraintDatabase.Database.DatabaseManager;
import edu.southern.ConstraintDatabase.Database.models.ColumnModel;
import edu.southern.ConstraintDatabase.Database.models.TableModel;
import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Grammer.GrammerLexer;
import edu.southern.ConstraintDatabase.Grammer.GrammerParser;
import edu.southern.ConstraintDatabase.TreeNodes.ClauseNode;
import edu.southern.ConstraintDatabase.TreeNodes.DBInfoNode;
import edu.southern.ConstraintDatabase.TreeNodes.TreeNode;
import edu.southern.ConstraintDatabase.Visitors.ConstraintNormalizationVisitor;
import edu.southern.ConstraintDatabase.Visitors.DatabaseFactSaverVisitor;
import edu.southern.ConstraintDatabase.Visitors.PrinterVisitor;
import edu.southern.ConstraintDatabase.Visitors.QueryPlanGenerationVisitor;
import edu.southern.ConstraintDatabase.Visitors.SemanticCheckingVisitor;
import edu.southern.ConstraintDatabase.Visitors.XMLPrinterVisitor;

public class LogicHandler {
	private DatabaseManager dm = null;
	public String currentDatabase = "Default.sqlite";
	public String currentDirectory = System.getProperty("user.dir");
	
	public LogicHandler() {
		dm = new DatabaseManager(currentDatabase, false);
	}
	
	public void selectDatabase(String filename) {
		currentDatabase = filename;
		useDatabase();
	}

	private void useDatabase() {
		dm.switchDatabase(currentDatabase);
	}
	
	public ClauseNode parseLine(String line) {
		GrammerLexer lex = new GrammerLexer(new ANTLRStringStream(line));
		CommonTokenStream tokens = new CommonTokenStream(lex);
		GrammerParser parser = new GrammerParser(tokens);
		
		try {
			ClauseNode c = parser.clause();
			if(parser.getNumberOfSyntaxErrors() == 0) {
				return c;
			} else {
				return null;
			}
		} catch (RecognitionException e) {
			return null;
		}
	}
	
	public List<ClauseNode> parseLines(String[] lines) {
		List<ClauseNode> ret = new ArrayList<ClauseNode>();
		boolean sucess = true;
		for(String s: lines) {
			if(s != null && s.length() > 0) {
				System.out.println(s);
				ClauseNode c = parseLine(s);
				ret.add(c);
				if(c == null) {
					sucess = false;
					break;
				}
			}
		}
		if(sucess) {
			System.out.println("Parse Successful");
			return ret;
		} else {
			System.out.println("Parse Failed");
			return null;
		}
	}
	
	
	public void normalizeClauseNode(TreeNode p) throws TreeNodeException {
		ConstraintNormalizationVisitor cnv = new ConstraintNormalizationVisitor();
		p.Accept(cnv);
	}
	
	public void normalize(List<ClauseNode> clauses) {
		boolean sucess = true;
		for(ClauseNode c: clauses) {
			try {
				PrinterVisitor pr = new PrinterVisitor(System.out);
				c.Accept(pr);
				normalizeClauseNode(c);
			} catch (TreeNodeException e) {
				System.out.println(e.getMessage());
				sucess = false;
				break;
			}
		}
		if(sucess) {
			System.out.println("Normalization Successful");
		} else {
			System.out.println("Normalization Failed");
		}
		
	}

	public void semanticCheck(TreeNode p) throws TreeNodeException {
		SemanticCheckingVisitor scv = new SemanticCheckingVisitor();
		p.Accept(scv);
	}
	
	public void semanticCheck(List<ClauseNode> clauses) {
		boolean sucess = true;
		for(ClauseNode c: clauses) {
			try {
				PrinterVisitor pr = new PrinterVisitor(System.out);
				c.Accept(pr);
				semanticCheck(c);
			} catch (TreeNodeException e) {
				System.out.println(e.getMessage());
				sucess = false;
				break;
			}
		}
		if(sucess) {
			System.out.println("Semantic Check Successful");
		} else {
			System.out.println("Semantic Check Failed");
		}
	}
	
	public List<String> saveClauseNode(TreeNode p) throws TreeNodeException {
		DatabaseFactSaverVisitor dsv = new DatabaseFactSaverVisitor(dm);
		p.Accept(dsv);
		return dsv.createdTables;
	}

	public List<String> save(List<ClauseNode> clauses) {
		boolean sucess = true;
		List<String> ret = new ArrayList<String>();
		for(ClauseNode c: clauses) {
			try {
				List<String> createdTables = saveClauseNode(c);
				ret.addAll(createdTables);
			} catch (TreeNodeException e) {
				System.out.println(e.getMessage());
				sucess = false;
				break;
			}
		}
		if(sucess) {
			System.out.println("Saving Clauses Successful");
		} else {
			System.out.println("Saving Clauses Failed");
		}
		return ret;
	}
	

	public void printClauseNode(TreeNode p) throws TreeNodeException {
		PrinterVisitor pr = new PrinterVisitor(System.out);
		p.Accept(pr);
	}

	public void printClauseNodeXML(TreeNode p) throws TreeNodeException {
		XMLPrinterVisitor xpr = new XMLPrinterVisitor(System.out);
		p.Accept(xpr);
	}
	
	public String generateQueryPlan(String tableName) {
		ClauseNode c = parseLine(tableName + ".");
		DBInfoNode db = c.subject.dbinfo;
		TableModel t = dm.getTableByName(db.varnamenode.varname);
		QueryPlanGenerationVisitor qpgv = new QueryPlanGenerationVisitor(dm);
		t.Accept(qpgv);
		
		String ret = null;
		String dotString = qpgv.getDotString();
		
		String time = (new Date()).getTime() + "";
		String tmpDir = System.getProperty("java.io.tmpdir");
		String tmpDotFilename = tmpDir + time + "_dot.gv";
		String tmpPNGFilename = tmpDir + time + "_dot.png";
		
		try {
			//System.out.println("Saving to: " + tmpDotFilename);
			PrintWriter out = new PrintWriter(tmpDotFilename);
			out.println(dotString);
			out.close();
			//System.out.println("dot -Tpng " + tmpDotFilename + " -o " + tmpPNGFilename);
			String[] cmd = { "/usr/local/bin/dot", "-Tpng", tmpDotFilename, "-o", tmpPNGFilename };
			Process p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
			ret = tmpPNGFilename;
		} catch (Exception e) {
			e.printStackTrace();
			new File(tmpPNGFilename).delete();
			ret = null;
		}
		new File(tmpDotFilename).delete();
		return ret;
	}
	
	public List<String> getDatabaseFiles() {
		List<String> ret = new ArrayList<String>();
		File dir = new File(currentDirectory);
		
		File[] files = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(".sqlite");
			}
		});
		for(File f: files) {
			ret.add(f.getName());
		}
		return ret;
	}

	public List<String> getTables() {
		List<String> ret = new ArrayList<String>();
		for(TableModel t: dm.getTables()) {
			String name = t.name + "(";
			int count = 0;
			for(ColumnModel c: t.columns) {
				name += c.name;
				if(count < t.columns.size() - 1) name += ",";
				count++;
			}
			name += ")";
			ret.add(name);
		}
		return ret;
	}

	public void setSelectedFolder(String selectedPath) {
		currentDirectory = selectedPath;
	}

	public void deleteDatabase(String filename) {
		dm.deleteDatabase(filename);
	}

	public void getTableRows(String string) {
		ClauseNode c = parseLine(string + ".");
		DBInfoNode db = c.subject.dbinfo;
		TableModel t = dm.getFullTable(db.varnamenode.varname);
		PrinterVisitor p = new PrinterVisitor(System.out);
		t.Accept(p);
	}




	
}
