package edu.southern.ConstraintDatabase.Visitors;

import java.util.HashMap;

import edu.southern.ConstraintDatabase.Exceptions.SemanticCheckingException;
import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;
import edu.southern.ConstraintDatabase.TreeNodes.AssignmentNode;
import edu.southern.ConstraintDatabase.TreeNodes.BoundNode;
import edu.southern.ConstraintDatabase.TreeNodes.ClauseNode;
import edu.southern.ConstraintDatabase.TreeNodes.ConstraintNode;
import edu.southern.ConstraintDatabase.TreeNodes.DBInfoNode;
import edu.southern.ConstraintDatabase.TreeNodes.ExpressionNode;
import edu.southern.ConstraintDatabase.TreeNodes.FactorNode;
import edu.southern.ConstraintDatabase.TreeNodes.PredicateNode;
import edu.southern.ConstraintDatabase.TreeNodes.StringNode;
import edu.southern.ConstraintDatabase.TreeNodes.SubjectNode;
import edu.southern.ConstraintDatabase.TreeNodes.TermNode;
import edu.southern.ConstraintDatabase.TreeNodes.VariableNode;

public class SemanticCheckingVisitor implements TreeNodeVisitor {
	
	private HashMap<String, String> sl = new HashMap<String, String>();
	private HashMap<String, String> dl = new HashMap<String, String>();
	private HashMap<String, String> cl = new HashMap<String, String>();
	
	
	private boolean inSubject = false;
	private boolean inDBInfo = false;
	private boolean inConstraint = false;
	
	
	public SemanticCheckingVisitor() {
	}

	public void Visit(ClauseNode programNode) throws TreeNodeException {
		programNode.subject.Accept(this);
		
		if(programNode.predicate != null) {
			programNode.predicate.Accept(this);

			for(String k: sl.keySet()) {
				if(!dl.containsKey(k) && !cl.containsKey(k)) {
					throw new SemanticCheckingException("Variable: " + k + " could not be found in referenced tables or constraints");
				}
			}
		
			for(String k: dl.keySet()) {
				if(dl.get(k).equals("1")) {
					if(!sl.containsKey(k) && !cl.containsKey(k)) {
						throw new SemanticCheckingException("Variable: " + k + " could not be found in subject table or constraints");
					}
				} else {
					// Could be the two input tables reference each other
				}
			} 

			for(String k: cl.keySet()) {
				if(!sl.containsKey(k) && !dl.containsKey(k)) {
					throw new SemanticCheckingException("Variable: " + k + " could not be found in subject table or referenced tables");
				}
			}
		}
	}


	public void Visit(SubjectNode subjectNode) throws TreeNodeException {
		inSubject = true;
		subjectNode.dbinfo.Accept(this);
		inSubject = false;
	}

	public void Visit(VariableNode varNode) throws TreeNodeException {
		if(varNode.isnum) return;
		boolean added = false;
		if(inSubject) {
			if(!sl.containsKey(varNode.varname)) {
				sl.put(varNode.varname, "1");
			} else {
				sl.put(varNode.varname, (Integer.parseInt(sl.get(varNode.varname)) + 1) + "");
			}
			added = true;
		}
		if(!inSubject && inDBInfo) {
			if(!dl.containsKey(varNode.varname)) {
				dl.put(varNode.varname, "1");
			} else {
				dl.put(varNode.varname, (Integer.parseInt(dl.get(varNode.varname)) + 1) + "");
				//dl[varNode.varname] = (int)dl[varNode.varname] + 1;
			}
			added = true;
		}
		if(inConstraint) {
			if(!cl.containsKey(varNode.varname)) {
				cl.put(varNode.varname, "1");
			} else {
				cl.put(varNode.varname, (Integer.parseInt(cl.get(varNode.varname)) + 1) + "");
				//cl[varNode.varname] = (int)cl[varNode.varname] + 1;
			}
			added = true;
		}
		if(!added) {
			throw new SemanticCheckingException("Error: Var: " + varNode.varname + " was not added to any list");
		}
	}

	public void Visit(PredicateNode predicateNode) throws TreeNodeException {
		for(DBInfoNode db: predicateNode.dbInfos) {
			db.Accept(this);
		}
		for(ConstraintNode c: predicateNode.constraintNodes) {
			c.Accept(this);
		}
	}
	public void Visit(DBInfoNode dbinfoNode) throws TreeNodeException {
		inDBInfo = true;
		for(VariableNode v: dbinfoNode.variables) {
			v.Accept(this);
		}
		inDBInfo = false;
	}
	public void Visit(ConstraintNode constraintNode) throws TreeNodeException {
		inConstraint = true;
		constraintNode.en.Accept(this);
		inConstraint = false;
	}
	public void Visit(ExpressionNode expessionNode) throws TreeNodeException {
		for(TermNode t: expessionNode.terms) {
			t.Accept(this);
		}
	}
	public void Visit(TermNode termNode) throws TreeNodeException {
		for(FactorNode f: termNode.factors) {
			f.Accept(this);
		}
	}
	public void Visit(FactorNode factorNode) throws TreeNodeException {
		if(factorNode.e != null) factorNode.e.Accept(this);
		if(factorNode.v != null) factorNode.v.Accept(this);
	}

	
	public void Visit(BoundNode boundNode) {

	}
	public void Visit(StringNode stringNode) {

	}
	public void Visit(AssignmentNode assignNode) {

	}
}
