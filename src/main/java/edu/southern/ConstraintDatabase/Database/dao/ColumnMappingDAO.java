package edu.southern.ConstraintDatabase.Database.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.southern.ConstraintDatabase.Database.models.ColumnMappingModel;
import edu.southern.ConstraintDatabase.Database.util.SQLiteConnection;

public class ColumnMappingDAO extends SQLiteDAO {
	
	public ColumnMappingDAO(SQLiteConnection con) {
		super(con);
		sqlTableName = "columnmappings";
	}
	
	public List<ColumnMappingModel> getColumnMappingsByTableRefId(long tableRefId) {
		List<ColumnMappingModel> list = new ArrayList<ColumnMappingModel>();
		ResultSet set = runQuery("select * from " + sqlTableName + " where tableRefId = " + tableRefId + " order by id");
		try {
			if(set.isBeforeFirst()) {
				while(set.next()) {
					list.add(populate(set));
				}
			}
			closeResultSet(set);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return list;
	}
	
	public ColumnMappingModel getColumnMappingById(long id) {
		ResultSet set = getRecordById(id);
		ColumnMappingModel cm = populate(set);
		closeResultSet(set);
		return cm;
	}
	
	public ColumnMappingModel createColumnMapping(long tableRefId, long columnId, String mappedName) {
		runUpdateQuery("insert into " + sqlTableName + " (tableRefId, columnId, mappedName) values(" + tableRefId + "," + columnId + ",'" + mappedName + "')");
		ResultSet set = runQuery("select * from " + sqlTableName + " where id = last_insert_rowid()");
		ColumnMappingModel cm = null;
		try {
			if(set.isBeforeFirst()) {
				while(set.next()) {
					cm = populate(set);
					break;
				}
			}
			closeResultSet(set);
		} catch (SQLException e) {
			cm = null;
			e.printStackTrace();
		}
		return cm;
	}
	

	public void deleteColumnMappingsByTableRefId(long tableRefId) {
		runUpdateQuery("delete from " + sqlTableName + " where tableRefId=" + tableRefId + "");
	}

	private ColumnMappingModel populate(ResultSet set) {
		ColumnMappingModel cm = new ColumnMappingModel();
		try {
			cm.id = set.getLong(1);
			cm.tableRefId = set.getLong(2);
			cm.columnId = set.getLong(3);
			cm.mappedName = set.getString(4);
		} catch (SQLException e) {
			e.printStackTrace();
			cm = null;
		}
		return cm;
	}
}
