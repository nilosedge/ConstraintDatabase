package edu.southern.ConstraintDatabase.Database.models;

import edu.southern.ConstraintDatabase.Interfaces.DatabaseDAOVisitor;

public class ColumnMappingModel extends DatabaseModel {
	public long id;
	public long tableRefId;
	public long columnId;
	public String mappedName;
	public ColumnModel column;
	
	public ColumnMappingModel() {
		
	}

	@Override
	public void Accept(DatabaseDAOVisitor visitor) {
		visitor.Visit(this);
	}
}
