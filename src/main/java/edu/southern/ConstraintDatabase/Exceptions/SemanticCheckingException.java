package edu.southern.ConstraintDatabase.Exceptions;

public class SemanticCheckingException extends TreeNodeException {

	private static final long serialVersionUID = -8739619324917201672L;

	public SemanticCheckingException(String str) {
		super(str);
	}
}
