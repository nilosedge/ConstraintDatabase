package edu.southern.ConstraintDatabase.Database;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import edu.southern.ConstraintDatabase.Database.models.ColumnMappingModel;
import edu.southern.ConstraintDatabase.Database.models.ColumnModel;
import edu.southern.ConstraintDatabase.Database.models.ConstraintModel;
import edu.southern.ConstraintDatabase.Database.models.RowModel;
import edu.southern.ConstraintDatabase.Database.models.TableModel;
import edu.southern.ConstraintDatabase.Database.models.TableReferenceModel;
import edu.southern.ConstraintDatabase.TreeNodes.ConstraintNode;
import edu.southern.ConstraintDatabase.TreeNodes.DBInfoNode;
import edu.southern.ConstraintDatabase.TreeNodes.Mapping.TreeNodeMapper;

public class DatabaseManager extends DatabaseCreator {

	private TreeNodeMapper mapper = new TreeNodeMapper(true);
	
	public DatabaseManager(String filename, boolean debug) {
		super(filename, debug);
	}
	
	public TableModel createTable(DBInfoNode dbInfoNode) {
		String tableName = dbInfoNode.varnamenode.varname;
		TableModel tm = tableDAO.getTableByName(tableName);
		if(tm == null) {
			tm = tableDAO.createTable(tableName);
		}
		return tm;
	}
	
	public TableReferenceModel createTableReference(long tableId, long rowId) {
		return tableRefDAO.createTableReference(tableId, rowId);
	}
	
	public ColumnModel createColumn(TableModel tm, String varname) {
		ColumnModel cm = columnDAO.columnExists(tm.id, varname);
		if(cm == null) {
			cm = columnDAO.createColumn(tm.id, varname);
		}
		return cm;
	}
	
	public RowModel createRow(TableModel tm) {
		return rowDAO.createRow(tm.id);
	}

	public ConstraintModel createConstraint(RowModel rm, String serializedObject) {
		return constraintDAO.createConstraint(rm.id, serializedObject);
	}

	public void createColumnMapping(long tableRefId, long columnId, String mappedName) {
		columnMapDAO.createColumnMapping(tableRefId, columnId, mappedName);
	}

	public TableModel getTableByName(String name) {
		return tableDAO.getTableByName(name);
	}
	
	public TableModel getTableById(long tableId) {
		return tableDAO.getTableById(tableId);
	}

	public List<TableModel> getTables() {
		List<TableModel> ret = tableDAO.getTables();
		for(TableModel t: ret) {
			List<ColumnModel> cols = columnDAO.getColumnsByTableId(t.id);
			t.columns = cols;
		}
		return ret;
	}
	
	public List<ColumnModel> getColumns(long tableId) {
		return columnDAO.getColumnsByTableId(tableId);
	}
	
	public List<RowModel> getRows(long tableId) {
		return rowDAO.getRowsByTableId(tableId);
	}
	
	public List<TableReferenceModel> getTableRefs(long rowId) {
		return tableRefDAO.getTableReferencesByRowId(rowId);
	}

	public void removeTable(String name) {
		TableModel tm = tableDAO.tableExists(name);
		if(tm == null) {
			return;
		}
		for( RowModel r: tm.rows) {
			constraintDAO.deleteConstraintsByRowId(r.id);
		}
		rowDAO.deleteRowsByTableId(tm.id);
		columnDAO.deleteColumnsByTableId(tm.id);
		tableDAO.deleteTableByName(name);
		return;
	}

	public TableModel getFullTable(String name) {
		TableModel t = tableDAO.getTableByName(name);
		t.columns = columnDAO.getColumnsByTableId(t.id);
		t.rows = rowDAO.getRowsByTableId(t.id);
		
		for(RowModel r: t.rows) {
			r.constraints = constraintDAO.getConstraintsByRowId(r.id);
			for(ConstraintModel c: r.constraints) {
				try {
					c.constraintNode = mapper.readValue(c.dataField, ConstraintNode.class);
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			r.refs = tableRefDAO.getTableReferencesByRowId(r.id);
			for(TableReferenceModel tr: r.refs) {
				tr.tableRef = tableDAO.getTableById(tr.tableId);
				tr.columnmappings = columnMapDAO.getColumnMappingsByTableRefId(tr.id);
				for(ColumnMappingModel map: tr.columnmappings) {
					map.column = columnDAO.getColumnById(map.columnId);
				}
			}
		}
		return t;
	}

}
