package edu.southern.ConstraintDatabase.Exceptions;

import org.antlr.runtime.RecognitionException;

public class RecognitionTreeNodeException extends RecognitionException {

	private static final long serialVersionUID = -2693871934013491628L;
	public String[] tokenNames;
	public RecognitionException recognitionException;
	
	public RecognitionTreeNodeException(String[] tokenNames, RecognitionException e) {
		this.tokenNames = tokenNames;
		this.recognitionException = e;
	}

}
