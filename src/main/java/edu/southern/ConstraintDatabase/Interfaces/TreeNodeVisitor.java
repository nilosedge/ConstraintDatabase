package edu.southern.ConstraintDatabase.Interfaces;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.TreeNodes.AssignmentNode;
import edu.southern.ConstraintDatabase.TreeNodes.BoundNode;
import edu.southern.ConstraintDatabase.TreeNodes.ClauseNode;
import edu.southern.ConstraintDatabase.TreeNodes.ConstraintNode;
import edu.southern.ConstraintDatabase.TreeNodes.DBInfoNode;
import edu.southern.ConstraintDatabase.TreeNodes.ExpressionNode;
import edu.southern.ConstraintDatabase.TreeNodes.FactorNode;
import edu.southern.ConstraintDatabase.TreeNodes.PredicateNode;
import edu.southern.ConstraintDatabase.TreeNodes.StringNode;
import edu.southern.ConstraintDatabase.TreeNodes.SubjectNode;
import edu.southern.ConstraintDatabase.TreeNodes.TermNode;
import edu.southern.ConstraintDatabase.TreeNodes.VariableNode;

public interface TreeNodeVisitor {
	void Visit (ClauseNode programNode) throws TreeNodeException;
	void Visit (SubjectNode subjectNode) throws TreeNodeException;
	void Visit (PredicateNode predicateNode) throws TreeNodeException;
	void Visit (DBInfoNode dbinfoNode) throws TreeNodeException;
	void Visit (BoundNode boundNode) throws TreeNodeException;
	void Visit (ConstraintNode constraintNode) throws TreeNodeException;
	void Visit (ExpressionNode expessionNode) throws TreeNodeException;
	void Visit (TermNode termNode) throws TreeNodeException;
	void Visit (StringNode stringNode) throws TreeNodeException;
	void Visit (VariableNode varNode) throws TreeNodeException;
	void Visit (AssignmentNode assignNode) throws TreeNodeException;
	void Visit (FactorNode factorNode) throws TreeNodeException;
}
