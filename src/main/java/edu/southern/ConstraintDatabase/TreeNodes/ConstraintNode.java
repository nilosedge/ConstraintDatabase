package edu.southern.ConstraintDatabase.TreeNodes;

import org.codehaus.jackson.annotate.JsonIgnore;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;

public class ConstraintNode extends TreeNode {

	public ExpressionNode en;
	public AssignmentNode an;
	public BoundNode bn;
	
	public ConstraintNode() {
	}
	
	public ConstraintNode(ExpressionNode en, AssignmentNode an, BoundNode bn) {
		this.en = en;
		this.an = an;
		this.bn = bn;
	}
	
	@JsonIgnore
	public boolean isNormalized() {
		return an.isNormalized();
	}
	
	public void makeNegative() {
		en.makeNegative();
		bn.makeNegative();
	}

	@Override
	public void Accept(TreeNodeVisitor visitor) throws TreeNodeException {
		visitor.Visit(this);
	}
}
