package edu.southern.ConstraintDatabase.Database.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.southern.ConstraintDatabase.Database.models.ConstraintModel;
import edu.southern.ConstraintDatabase.Database.util.SQLiteConnection;

public class ConstraintDAO extends SQLiteDAO {

	public ConstraintDAO(SQLiteConnection con) {
		super(con);
		sqlTableName = "constraints";
	}
	
	public List<ConstraintModel> getConstraintsByRowId(long rowId) {
		List<ConstraintModel> list = new ArrayList<ConstraintModel>();
		ResultSet set = runQuery("select * from " + sqlTableName + " where rowId = " + rowId + " order by id");
		try {
			if(set.isBeforeFirst()) {
				while(set.next()) {
					list.add(populate(set));
				}
			}
			closeResultSet(set);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return list;
	}
	
	public ConstraintModel getConstraintById(long id) {
		ResultSet set = getRecordById(id);
		ConstraintModel c = populate(set);
		closeResultSet(set);
		return c;
	}
	
	public ConstraintModel createConstraint(long rowId, String dataField) {
		runUpdateQueryWithParam("insert into " + sqlTableName + " (rowId, dataField) values(" + rowId + ", ?)", dataField);
		ResultSet set = runQuery("select * from " + sqlTableName + " where id = last_insert_rowid()");
		ConstraintModel c = null;
		try {
			if(set.isBeforeFirst()) {
				while(set.next()) {
					c = populate(set);
					break;
				}
			}
			closeResultSet(set);
		} catch (SQLException e) {
			c = null;
			e.printStackTrace();
		}
		return c;
	}
	
	public void deleteConstraintsByRowId(long rowId) {
		runUpdateQuery("delete from " + sqlTableName + " where rowId=" + rowId + "");
	}
	
	private ConstraintModel populate(ResultSet set) {
		ConstraintModel c = new ConstraintModel();
		try {
			c.id = set.getLong(1);
			c.rowId = set.getLong(2);
			c.dataField = set.getString(3);
		} catch (SQLException e) {
			e.printStackTrace();
			c = null;
		}
		return c;
	}
}
