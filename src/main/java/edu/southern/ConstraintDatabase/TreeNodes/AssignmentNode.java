package edu.southern.ConstraintDatabase.TreeNodes;

import org.codehaus.jackson.annotate.JsonIgnore;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;
import edu.southern.ConstraintDatabase.enums.AssignmentType;

public class AssignmentNode extends TreeNode {

	public AssignmentType type;
	
	public AssignmentNode() {
	}
	
	public AssignmentNode(AssignmentType type) {
		this.type = type;
	}
	
	public void Accept(TreeNodeVisitor visitor) throws TreeNodeException {
		visitor.Visit(this);
	}

	@JsonIgnore
	public boolean isNormalized() {
		if(type == AssignmentType.GreaterThan || type == AssignmentType.GreaterThanOrEqualTo) {
			return true;
		} else {
			return false;
		}
	}

}