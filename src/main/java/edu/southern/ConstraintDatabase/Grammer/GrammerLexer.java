// $ANTLR 3.5 /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g 2013-05-14 16:52:41

package edu.southern.ConstraintDatabase.Grammer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class GrammerLexer extends Lexer {
	public static final int EOF=-1;
	public static final int T__22=22;
	public static final int T__23=23;
	public static final int COMMA=4;
	public static final int DIGIT=5;
	public static final int DIV=6;
	public static final int EQ=7;
	public static final int GEQ=8;
	public static final int GT=9;
	public static final int LEQ=10;
	public static final int LPAREN=11;
	public static final int LT=12;
	public static final int MINUS=13;
	public static final int MULT=14;
	public static final int NEQ=15;
	public static final int NEWLINE=16;
	public static final int PLUS=17;
	public static final int QUOTED=18;
	public static final int RPAREN=19;
	public static final int VARNAME=20;
	public static final int WS=21;

	@Override
	public void emitErrorMessage(String msg) {
		//super.emitErrorMessage(msg);
	}

	@Override
	public void displayRecognitionError(String[] tokenNames, RecognitionException e) {
		super.displayRecognitionError(tokenNames, e);
		String hdr = getErrorHeader(e);
		String msg = getErrorMessage(e, tokenNames);
		String errorMessage = "";
		for (int i = 0; i < e.charPositionInLine; i++) errorMessage += " ";
		errorMessage += "^";
		System.out.println(errorMessage);
		System.out.println("Lexer: An error occured on: " + hdr + " " + msg + "");
	}


	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public GrammerLexer() {} 
	public GrammerLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public GrammerLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "/Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g"; }

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:29:7: ( ',' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:29:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:30:5: ( '/' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:30:7: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	// $ANTLR start "EQ"
	public final void mEQ() throws RecognitionException {
		try {
			int _type = EQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:31:4: ( '=' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:31:6: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EQ"

	// $ANTLR start "GEQ"
	public final void mGEQ() throws RecognitionException {
		try {
			int _type = GEQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:32:5: ( '>=' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:32:7: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GEQ"

	// $ANTLR start "GT"
	public final void mGT() throws RecognitionException {
		try {
			int _type = GT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:33:4: ( '>' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:33:6: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "GT"

	// $ANTLR start "LEQ"
	public final void mLEQ() throws RecognitionException {
		try {
			int _type = LEQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:34:5: ( '<=' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:34:7: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LEQ"

	// $ANTLR start "LPAREN"
	public final void mLPAREN() throws RecognitionException {
		try {
			int _type = LPAREN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:35:8: ( '(' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:35:10: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LPAREN"

	// $ANTLR start "LT"
	public final void mLT() throws RecognitionException {
		try {
			int _type = LT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:36:4: ( '<' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:36:6: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LT"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:37:7: ( '-' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:37:9: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "MULT"
	public final void mMULT() throws RecognitionException {
		try {
			int _type = MULT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:38:6: ( '*' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:38:8: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MULT"

	// $ANTLR start "NEQ"
	public final void mNEQ() throws RecognitionException {
		try {
			int _type = NEQ;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:39:5: ( '!=' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:39:7: '!='
			{
			match("!="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEQ"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:40:6: ( '+' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:40:8: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "RPAREN"
	public final void mRPAREN() throws RecognitionException {
		try {
			int _type = RPAREN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:41:8: ( ')' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:41:10: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RPAREN"

	// $ANTLR start "T__22"
	public final void mT__22() throws RecognitionException {
		try {
			int _type = T__22;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:42:7: ( '.' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:42:9: '.'
			{
			match('.'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__22"

	// $ANTLR start "T__23"
	public final void mT__23() throws RecognitionException {
		try {
			int _type = T__23;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:43:7: ( ':-' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:43:9: ':-'
			{
			match(":-"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__23"

	// $ANTLR start "DIGIT"
	public final void mDIGIT() throws RecognitionException {
		try {
			int _type = DIGIT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:164:6: ( ( '0' .. '9' ) )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIGIT"

	// $ANTLR start "VARNAME"
	public final void mVARNAME() throws RecognitionException {
		try {
			int _type = VARNAME;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:166:8: ( ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )* )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:167:2: ( 'a' .. 'z' | 'A' .. 'Z' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:167:21: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VARNAME"

	// $ANTLR start "NEWLINE"
	public final void mNEWLINE() throws RecognitionException {
		try {
			int _type = NEWLINE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:169:8: ( ( '\\r' )? '\\n' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:170:2: ( '\\r' )? '\\n'
			{
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:170:2: ( '\\r' )?
			int alt2=2;
			int LA2_0 = input.LA(1);
			if ( (LA2_0=='\r') ) {
				alt2=1;
			}
			switch (alt2) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:170:2: '\\r'
					{
					match('\r'); 
					}
					break;

			}

			match('\n'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NEWLINE"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:171:3: ( ( ' ' | '\\t' | '\\n' | '\\r' )+ )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:172:2: ( ' ' | '\\t' | '\\n' | '\\r' )+
			{
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:172:2: ( ' ' | '\\t' | '\\n' | '\\r' )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= '\t' && LA3_0 <= '\n')||LA3_0=='\r'||LA3_0==' ') ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}

			 _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "QUOTED"
	public final void mQUOTED() throws RecognitionException {
		try {
			int _type = QUOTED;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:178:7: ( '\"' (~ '\"' )* '\"' )
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:178:9: '\"' (~ '\"' )* '\"'
			{
			match('\"'); 
			// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:178:13: (~ '\"' )*
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( ((LA4_0 >= '\u0000' && LA4_0 <= '!')||(LA4_0 >= '#' && LA4_0 <= '\uFFFF')) ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop4;
				}
			}

			match('\"'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "QUOTED"

	@Override
	public void mTokens() throws RecognitionException {
		// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:8: ( COMMA | DIV | EQ | GEQ | GT | LEQ | LPAREN | LT | MINUS | MULT | NEQ | PLUS | RPAREN | T__22 | T__23 | DIGIT | VARNAME | NEWLINE | WS | QUOTED )
		int alt5=20;
		switch ( input.LA(1) ) {
		case ',':
			{
			alt5=1;
			}
			break;
		case '/':
			{
			alt5=2;
			}
			break;
		case '=':
			{
			alt5=3;
			}
			break;
		case '>':
			{
			int LA5_4 = input.LA(2);
			if ( (LA5_4=='=') ) {
				alt5=4;
			}

			else {
				alt5=5;
			}

			}
			break;
		case '<':
			{
			int LA5_5 = input.LA(2);
			if ( (LA5_5=='=') ) {
				alt5=6;
			}

			else {
				alt5=8;
			}

			}
			break;
		case '(':
			{
			alt5=7;
			}
			break;
		case '-':
			{
			alt5=9;
			}
			break;
		case '*':
			{
			alt5=10;
			}
			break;
		case '!':
			{
			alt5=11;
			}
			break;
		case '+':
			{
			alt5=12;
			}
			break;
		case ')':
			{
			alt5=13;
			}
			break;
		case '.':
			{
			alt5=14;
			}
			break;
		case ':':
			{
			alt5=15;
			}
			break;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			{
			alt5=16;
			}
			break;
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'I':
		case 'J':
		case 'K':
		case 'L':
		case 'M':
		case 'N':
		case 'O':
		case 'P':
		case 'Q':
		case 'R':
		case 'S':
		case 'T':
		case 'U':
		case 'V':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z':
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'f':
		case 'g':
		case 'h':
		case 'i':
		case 'j':
		case 'k':
		case 'l':
		case 'm':
		case 'n':
		case 'o':
		case 'p':
		case 'q':
		case 'r':
		case 's':
		case 't':
		case 'u':
		case 'v':
		case 'w':
		case 'x':
		case 'y':
		case 'z':
			{
			alt5=17;
			}
			break;
		case '\r':
			{
			int LA5_16 = input.LA(2);
			if ( (LA5_16=='\n') ) {
				int LA5_17 = input.LA(3);
				if ( ((LA5_17 >= '\t' && LA5_17 <= '\n')||LA5_17=='\r'||LA5_17==' ') ) {
					alt5=19;
				}

				else {
					alt5=18;
				}

			}

			else {
				alt5=19;
			}

			}
			break;
		case '\n':
			{
			int LA5_17 = input.LA(2);
			if ( ((LA5_17 >= '\t' && LA5_17 <= '\n')||LA5_17=='\r'||LA5_17==' ') ) {
				alt5=19;
			}

			else {
				alt5=18;
			}

			}
			break;
		case '\t':
		case ' ':
			{
			alt5=19;
			}
			break;
		case '\"':
			{
			alt5=20;
			}
			break;
		default:
			NoViableAltException nvae =
				new NoViableAltException("", 5, 0, input);
			throw nvae;
		}
		switch (alt5) {
			case 1 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:10: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 2 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:16: DIV
				{
				mDIV(); 

				}
				break;
			case 3 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:20: EQ
				{
				mEQ(); 

				}
				break;
			case 4 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:23: GEQ
				{
				mGEQ(); 

				}
				break;
			case 5 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:27: GT
				{
				mGT(); 

				}
				break;
			case 6 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:30: LEQ
				{
				mLEQ(); 

				}
				break;
			case 7 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:34: LPAREN
				{
				mLPAREN(); 

				}
				break;
			case 8 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:41: LT
				{
				mLT(); 

				}
				break;
			case 9 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:44: MINUS
				{
				mMINUS(); 

				}
				break;
			case 10 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:50: MULT
				{
				mMULT(); 

				}
				break;
			case 11 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:55: NEQ
				{
				mNEQ(); 

				}
				break;
			case 12 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:59: PLUS
				{
				mPLUS(); 

				}
				break;
			case 13 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:64: RPAREN
				{
				mRPAREN(); 

				}
				break;
			case 14 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:71: T__22
				{
				mT__22(); 

				}
				break;
			case 15 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:77: T__23
				{
				mT__23(); 

				}
				break;
			case 16 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:83: DIGIT
				{
				mDIGIT(); 

				}
				break;
			case 17 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:89: VARNAME
				{
				mVARNAME(); 

				}
				break;
			case 18 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:97: NEWLINE
				{
				mNEWLINE(); 

				}
				break;
			case 19 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:105: WS
				{
				mWS(); 

				}
				break;
			case 20 :
				// /Users/olinblodgett/git/ConstraintDatabase/src/main/java/edu/southern/ConstraintDatabase/Grammer/Grammer.g:1:108: QUOTED
				{
				mQUOTED(); 

				}
				break;

		}
	}



}
