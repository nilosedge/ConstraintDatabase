package edu.southern.ConstraintDatabase.Database.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.southern.ConstraintDatabase.Database.models.TableModel;
import edu.southern.ConstraintDatabase.Database.util.SQLiteConnection;

public class TableDAO extends SQLiteDAO {

	public TableDAO(SQLiteConnection con) {
		super(con);
		sqlTableName = "tables";
	}
	
	public List<TableModel> getTables() {
		List<TableModel> list = new ArrayList<TableModel>();
		ResultSet set = runQuery("select * from " + sqlTableName + " order by id");
		try {
			if(set.isBeforeFirst()) {
				while(set.next()) {
					list.add(populate(set));
				}
			}
			closeResultSet(set);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return list;
	}
	
	public TableModel getTableById(long id) {
		ResultSet set = getRecordById(id);
		TableModel t = null;
		try {
			if(set.isBeforeFirst()) {
				while(set.next()) {
					t = populate(set);
					break;
				}
			}
			closeResultSet(set);
		} catch (SQLException e) {
			t = null;
			e.printStackTrace();
		}
		return t;
	}
	
	public TableModel getTableByName(String name) {
		ResultSet set = getRecordByName(name);
		TableModel t = null;
		try {
			if(set.isBeforeFirst()) {
				while(set.next()) {
					t = populate(set);
					break;
				}
			}
			closeResultSet(set);
		} catch (SQLException e) {
			t = null;
			e.printStackTrace();
		}
		return t;
	}
	
	public TableModel tableExists(String name) {
		return getTableByName(name);
	}
	
	public TableModel createTable(String name) {
		runUpdateQuery("insert into " + sqlTableName + " (name) values('" + name + "')");
		return getTableByName(name);
	}
	
	public void deleteTableById(long id) {
		runUpdateQuery("delete from " + sqlTableName + " where id=" + id);
	}
	
	public void deleteTableByName(String name) {
		runUpdateQuery("delete from " + sqlTableName + " where name='" + name + "'");
	}
	
	private TableModel populate(ResultSet set) {
		TableModel t = new TableModel();
		try {
			t.id = set.getLong(1);
			t.name = set.getString(2);
		} catch (SQLException e) {
			e.printStackTrace();
			t = null;
		}
		return t;
	}
}
