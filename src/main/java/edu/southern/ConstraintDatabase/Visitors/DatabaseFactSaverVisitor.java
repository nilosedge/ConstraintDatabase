package edu.southern.ConstraintDatabase.Visitors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;

import edu.southern.ConstraintDatabase.Database.DatabaseManager;
import edu.southern.ConstraintDatabase.Database.models.ColumnModel;
import edu.southern.ConstraintDatabase.Database.models.ConstraintModel;
import edu.southern.ConstraintDatabase.Database.models.RowModel;
import edu.southern.ConstraintDatabase.Database.models.TableModel;
import edu.southern.ConstraintDatabase.Database.models.TableReferenceModel;
import edu.southern.ConstraintDatabase.Exceptions.DatabaseFactSaverException;
import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;
import edu.southern.ConstraintDatabase.TreeNodes.AssignmentNode;
import edu.southern.ConstraintDatabase.TreeNodes.BoundNode;
import edu.southern.ConstraintDatabase.TreeNodes.ClauseNode;
import edu.southern.ConstraintDatabase.TreeNodes.ConstraintNode;
import edu.southern.ConstraintDatabase.TreeNodes.DBInfoNode;
import edu.southern.ConstraintDatabase.TreeNodes.ExpressionNode;
import edu.southern.ConstraintDatabase.TreeNodes.FactorNode;
import edu.southern.ConstraintDatabase.TreeNodes.PredicateNode;
import edu.southern.ConstraintDatabase.TreeNodes.StringNode;
import edu.southern.ConstraintDatabase.TreeNodes.SubjectNode;
import edu.southern.ConstraintDatabase.TreeNodes.TermNode;
import edu.southern.ConstraintDatabase.TreeNodes.VariableNode;
import edu.southern.ConstraintDatabase.TreeNodes.Mapping.TreeNodeMapper;

public class DatabaseFactSaverVisitor implements TreeNodeVisitor {

	private DatabaseManager dm;
	private TableModel tm;
	private RowModel rm;
	private TreeNodeMapper mapper = new TreeNodeMapper(true);
	public List<String> createdTables = new ArrayList<String>();

	public DatabaseFactSaverVisitor(DatabaseManager dm) {
		this.dm = dm;
	}

	public void Visit(ClauseNode clauseNode) throws TreeNodeException {
		clauseNode.subject.Accept(this);
		if(clauseNode.predicate != null) {
			clauseNode.predicate.Accept(this);
		}
	}

	public void Visit(SubjectNode subjectNode) throws TreeNodeException {
		tm = dm.createTable(subjectNode.dbinfo);
		if(tm == null) {
			throw new DatabaseFactSaverException("Error Creating Table: " + subjectNode.dbinfo.varnamenode.varname);
		}

		List<ColumnModel> list = dm.getColumns(tm.id);
		
		if(list.size() > 0) {
			// Do the database checks
			if(list.size() != subjectNode.dbinfo.variables.size()) {
				throw new DatabaseFactSaverException("Error Saving Columns: Columns Counts miss match: Database has " + list.size() + " columns and you were trying to add the same table name with " + subjectNode.dbinfo.variables.size() + " columns.");
			}
			int c = 0;
			for(VariableNode v: subjectNode.dbinfo.variables) {
				if(!list.get(c).name.equals(v.varname)) {
					throw new DatabaseFactSaverException("Error Saving Columns: Columns Miss Matched Names: Column " + list.get(c).name + " does not equal " + v.varname + " please change names and resubmit.");
				}
				c++;
			}
		} else {
			// Insert them all as new
			String s = tm.name + "(";
			int c = 0;
			for(VariableNode v: subjectNode.dbinfo.variables) {
				v.Accept(this);
				s += v.varname;
				if(c < subjectNode.dbinfo.variables.size() - 1) s += ",";
				c++;
			}
			s += ")";
			createdTables.add(s);
		}

	}

	public void Visit(PredicateNode predicateNode) throws TreeNodeException {
		for(DBInfoNode db: predicateNode.dbInfos) {
			db.Accept(this);
		}
		for(ConstraintNode c: predicateNode.constraintNodes) {
			c.Accept(this);
		}
	}

	public void Visit(DBInfoNode dbinfoNode) throws TreeNodeException {


		TableModel tableRef = dm.getTableByName(dbinfoNode.varnamenode.varname);
		if(tableRef == null) {
			throw new DatabaseFactSaverException("Error Creating Table Reference: Table " + dbinfoNode.varnamenode.varname + " does not exist please define before using in statements.");
		}

		if(rm == null) {
			rm = dm.createRow(tm);
			if(rm == null) {
				System.out.println("Error Creating Row: ");
			}
		}

		List<ColumnModel> list = dm.getColumns(tableRef.id);

		if(list.size() != dbinfoNode.variables.size()) {
			throw new DatabaseFactSaverException("Error Saving Table Reference: Columns Count miss match: Database has " + list.size() + " columns and you were trying to reference same table name with " + dbinfoNode.variables.size() + " columns.");
		}

		TableReferenceModel trm = dm.createTableReference(tableRef.id, rm.id);
	
		int count = 0;
		for(ColumnModel c: list) {
			dm.createColumnMapping(trm.id, c.id, dbinfoNode.variables.get(count).varname);
			count++;
		}

	}

	public void Visit(VariableNode varNode) throws TreeNodeException {
		ColumnModel cm = dm.createColumn(tm, varNode.varname);
		if(cm == null) {
			throw new DatabaseFactSaverException("Error Creating Column: " + varNode.varname);
		}
	}
	
	public void Visit(ConstraintNode constraintNode) throws TreeNodeException {
		if(rm == null) {
			rm = dm.createRow(tm);
			if(rm == null) {
				throw new DatabaseFactSaverException("Error Creating Row: ");
			}
		}

		String dataField = "";
		try {
			dataField = mapper.writeValueAsString(constraintNode);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ConstraintModel cm = dm.createConstraint(rm, dataField);
		if(cm == null) {
			throw new DatabaseFactSaverException("Error Creating Constraint: " + dataField);
		}

	}

	public void Visit(AssignmentNode assignNode) {

	}
	public void Visit(BoundNode boundNode) {

	}
	public void Visit(ExpressionNode expessionNode) {

	}
	public void Visit(TermNode termNode) {

	}
	public void Visit(StringNode stringNode) {

	}
	public void Visit(FactorNode factorNode) {

	}
}
