package edu.southern.ConstraintDatabase.Database.models;

import java.util.ArrayList;
import java.util.List;

import edu.southern.ConstraintDatabase.Interfaces.DatabaseDAOVisitor;

public class TableModel extends DatabaseModel {
	
	public long id;
	public String name;
	public List<ColumnModel> columns = new ArrayList<ColumnModel>();
	public List<RowModel> rows = new ArrayList<RowModel>();
	
	public TableModel() {
	}

	public void Accept(DatabaseDAOVisitor visitor) {
		visitor.Visit(this);
	}

}
