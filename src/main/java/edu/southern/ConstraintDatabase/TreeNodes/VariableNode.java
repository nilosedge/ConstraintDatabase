package edu.southern.ConstraintDatabase.TreeNodes;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;

public class VariableNode extends TreeNode {

	public boolean neg;
	public boolean sb;
	public boolean isen;
	public boolean isnum;
	public String varname;
	public ExpressionNode e;
	
	public VariableNode() {
	}
	
	public VariableNode(String varname, boolean isnum) {
		this.varname = varname;
		this.isnum = isnum;
	}
	
	public VariableNode(ExpressionNode e) {
		isen = true;
		this.e = e;
	}

	@Override
	public void Accept(TreeNodeVisitor visitor) throws TreeNodeException {
		visitor.Visit(this);
	}
	
}
