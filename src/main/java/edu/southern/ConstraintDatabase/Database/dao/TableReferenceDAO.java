package edu.southern.ConstraintDatabase.Database.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.southern.ConstraintDatabase.Database.models.TableReferenceModel;
import edu.southern.ConstraintDatabase.Database.util.SQLiteConnection;

public class TableReferenceDAO extends SQLiteDAO {

	public TableReferenceDAO(SQLiteConnection con) {
		super(con);
		sqlTableName = "tablerefs";
	}
	
	public List<TableReferenceModel> getTableReferencesByRowId(long rowId) {
		List<TableReferenceModel> list = new ArrayList<TableReferenceModel>();
		ResultSet set = runQuery("select * from " + sqlTableName + " where rowId = " + rowId + " order by id");
		try {
			if(set.isBeforeFirst()) {
				while(set.next()) {
					list.add(populate(set));
				}
			}
			closeResultSet(set);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		return list;
	}
	
	public TableReferenceModel getTableReferenceById(long id) {
		ResultSet set = getRecordById(id);
		TableReferenceModel tr = populate(set);
		closeResultSet(set);
		return tr;
	}
	
	public TableReferenceModel createTableReference(long tableId, long rowId) {
		runUpdateQuery("insert into " + sqlTableName + " (rowId, tableId) values(" + rowId + "," + tableId + ")");
		ResultSet set = runQuery("select * from " + sqlTableName + " where id = last_insert_rowid()");
		TableReferenceModel tr = null;
		try {
			if(set.isBeforeFirst()) {
				while(set.next()) {
					tr = populate(set);
					break;
				}
			}
			closeResultSet(set);
		} catch (SQLException e) {
			tr = null;
			e.printStackTrace();
		}
		return tr;
	}
	
	public void deleteTableReferenceByRowId(long rowId) {
		runUpdateQuery("delete from " + sqlTableName + " where rowId=" + rowId);
	}
	
	private TableReferenceModel populate(ResultSet set) {
		TableReferenceModel tr = new TableReferenceModel();
		try {
			tr.id = set.getLong(1);
			tr.rowId = set.getLong(2);
			tr.tableId = set.getLong(3);
		} catch (SQLException e) {
			e.printStackTrace();
			tr = null;
		}
		return tr;
	}
}
