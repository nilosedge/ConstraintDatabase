package edu.southern.ConstraintDatabase.Database.models;

import edu.southern.ConstraintDatabase.Interfaces.DatabaseDAOVisitor;

public class ColumnModel extends DatabaseModel {

	public long id;
	public long tableId;
	public String name;
	
	public ColumnModel() {
	}

	public void Accept(DatabaseDAOVisitor visitor) {
		visitor.Visit(this);
	}
}
