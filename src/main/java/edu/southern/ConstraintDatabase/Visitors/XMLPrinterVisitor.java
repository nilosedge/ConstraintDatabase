package edu.southern.ConstraintDatabase.Visitors;

import java.io.PrintStream;

import edu.southern.ConstraintDatabase.Database.models.ColumnModel;
import edu.southern.ConstraintDatabase.Database.models.ColumnMappingModel;
import edu.southern.ConstraintDatabase.Database.models.ConstraintModel;
import edu.southern.ConstraintDatabase.Database.models.RowModel;
import edu.southern.ConstraintDatabase.Database.models.TableModel;
import edu.southern.ConstraintDatabase.Database.models.TableReferenceModel;
import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.DatabaseDAOVisitor;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;
import edu.southern.ConstraintDatabase.TreeNodes.AssignmentNode;
import edu.southern.ConstraintDatabase.TreeNodes.BoundNode;
import edu.southern.ConstraintDatabase.TreeNodes.ClauseNode;
import edu.southern.ConstraintDatabase.TreeNodes.ConstraintNode;
import edu.southern.ConstraintDatabase.TreeNodes.DBInfoNode;
import edu.southern.ConstraintDatabase.TreeNodes.ExpressionNode;
import edu.southern.ConstraintDatabase.TreeNodes.FactorNode;
import edu.southern.ConstraintDatabase.TreeNodes.PredicateNode;
import edu.southern.ConstraintDatabase.TreeNodes.StringNode;
import edu.southern.ConstraintDatabase.TreeNodes.SubjectNode;
import edu.southern.ConstraintDatabase.TreeNodes.TermNode;
import edu.southern.ConstraintDatabase.TreeNodes.VariableNode;

public class XMLPrinterVisitor extends PrinterUtil implements TreeNodeVisitor, DatabaseDAOVisitor {

	public XMLPrinterVisitor (PrintStream out) {
		super(out);
	}

	public void Visit (ClauseNode programNode) throws TreeNodeException {
		PrintLine("<program>");
		indent();
		programNode.subject.Accept(this);
		unindent();
		PrintLine(":-");
		indent();
		programNode.predicate.Accept(this);
		unindent();
		PrintLine(".");
		PrintLine("</program>");
	}
	public void Visit (SubjectNode subjectNode) throws TreeNodeException {
		PrintLine("<subject>");
		indent();
		subjectNode.dbinfo.Accept(this);
		unindent();
		PrintLine("</subject>");
	}
	public void Visit (PredicateNode predicateNode) throws TreeNodeException {
		PrintLine("<predicate>");
		indent();
		for(DBInfoNode db: predicateNode.dbInfos) {
			db.Accept(this);
		}
		for(ConstraintNode con: predicateNode.constraintNodes) {
			con.Accept(this);
		}
		unindent();
		PrintLine("</predicate>");
	}
	public void Visit (DBInfoNode dbinfoNode) throws TreeNodeException {
		PrintLine("<dbinfo>");
		indent();
		dbinfoNode.varnamenode.Accept(this);
		for(VariableNode v:dbinfoNode.variables) {
			v.Accept(this);
		}
		unindent();
		PrintLine("</dbinfo>");
	}
	public void Visit (BoundNode boundNode) throws TreeNodeException {
		PrintLine("<bound>");
		indent();
		boundNode.varNode.Accept(this);
		unindent();
		PrintLine("</bound>");
	}
	public void Visit (ConstraintNode constraintNode) throws TreeNodeException {
		PrintLine("<constraint>");
		indent();
		constraintNode.en.Accept(this);
		constraintNode.an.Accept(this);
		constraintNode.bn.Accept(this);
		unindent();
		PrintLine("</constraint>");
	}
	public void Visit(ExpressionNode expessionNode) throws TreeNodeException {
		PrintLine("<expression>");
		indent();
		for(TermNode t:expessionNode.terms) {
			t.Accept(this);
		}
		unindent();
		PrintLine("</expression>");
	}
	public void Visit(TermNode termNode) throws TreeNodeException {
		if(termNode.neg) {
			PrintLine("<neg_term>");
		} else {
			PrintLine("<term>");
		}
		indent();
		int count = 0;
		for(FactorNode f: termNode.factors) {
			if(count > 0) {
				PrintLine(termNode.opers.get(count-1));
			}
			f.Accept(this);
			count++;
		}
		unindent();
		if(termNode.neg) {
			PrintLine("</neg_term>");
		} else {
			PrintLine("</term>");
		}

	}
	public void Visit(VariableNode varNode) {
		if(varNode.neg) {
			PrintLine("<neg_variable>" + varNode.varname + "</variable>");
		} else {
			PrintLine("<variable>" + varNode.varname + "</variable>");
		}
	}
	public void Visit(AssignmentNode assignNode) {
		PrintLine("<assignmentNode>");
		indent();
		PrintLine(assignNode.type.getOper());
		unindent();
		PrintLine("</assignmentNode>");
	}
	public void Visit (StringNode stringNode) {
		PrintLine("<string>" + stringNode.str + "</string>");
	}
	public void Visit(FactorNode factorNode) throws TreeNodeException {
		PrintLine("<factorNode>");
		indent();
		if(factorNode.e != null) factorNode.e.Accept(this);
		if(factorNode.v != null) factorNode.v.Accept(this);
		unindent();
		PrintLine("</factorNode>");
	}


	public void Visit(TableModel table) {
		PrintLine("<table id=\"" + table.id + "\" name=\"" + table.name + "\">");
		indent();
		for(ColumnModel c: table.columns) {
			c.Accept(this);
		}
		for(RowModel r: table.rows) {
			r.Accept(this);
		}
		unindent();
		PrintLine("</table>");
	}

	public void Visit(ColumnModel column) {
		PrintLine("<column id=\"" + column.id + "\" name=\"" + column.name + "\" />");
	}
	
	public void Visit(RowModel row) {
		PrintLine("<row>");
		indent();
		for(TableReferenceModel tr: row.refs) {
			tr.Accept(this);
		}
		for(ConstraintModel c: row.constraints) {
			c.Accept(this);
		}
		unindent();
		PrintLine("</row>");
	}
	
	public void Visit(ConstraintModel constraint) {
		try {
			constraint.constraintNode.Accept(this);
		} catch (TreeNodeException e) {
			e.printStackTrace();
		}
	}
	
	public void Visit(ColumnMappingModel columnMapping) {
		PrintLine("<columnMapping name=\"" + columnMapping.column.name + "\" mappedName=\"" + columnMapping.mappedName + "\">");
	}


	public void Visit(TableReferenceModel tableReference) {
		PrintLine("<tableReference name=\"" + tableReference.tableRef.name + "\">");
		indent();
		for(ColumnMappingModel cm: tableReference.columnmappings) {
			cm.Accept(this);
		}
		unindent();
		PrintLine("</tableReference>");
	}

}
