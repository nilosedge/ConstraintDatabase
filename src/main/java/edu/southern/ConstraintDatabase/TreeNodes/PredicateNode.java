package edu.southern.ConstraintDatabase.TreeNodes;

import java.util.ArrayList;
import java.util.List;

import edu.southern.ConstraintDatabase.Exceptions.TreeNodeException;
import edu.southern.ConstraintDatabase.Interfaces.TreeNodeVisitor;

public class PredicateNode extends TreeNode {

	public List<DBInfoNode> dbInfos = new ArrayList<DBInfoNode>();
	public List<ConstraintNode> constraintNodes = new ArrayList<ConstraintNode>();
	
	public PredicateNode() {
	}
	
	public void addDBInfo(DBInfoNode db) {
		dbInfos.add(db);
	}
	
	public void addConstraint(ConstraintNode con) {
		constraintNodes.add(con);
	}
	
	public boolean isFact() {
		if(dbInfos.size() > 0) {
			return false;
		} else return true;
	}

	@Override
	public void Accept(TreeNodeVisitor visitor) throws TreeNodeException {
		visitor.Visit(this);
	}
}
